<?php declare(strict_types = 1);

require __DIR__ . '/vendor/autoload.php';

/** @var \Nette\DI\Container $container */
$container = App\Bootstrap::boot()
	->createContainer();

/** @var \Mdfx\Doctrine\Model\EntityManager $entityManager */
$entityManager = $container->getByType(\Mdfx\Doctrine\Model\EntityManager::class);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);
