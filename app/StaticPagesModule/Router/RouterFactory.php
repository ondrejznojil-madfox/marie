<?php declare(strict_types = 1);

namespace App\StaticPagesModule\Router;

final class RouterFactory
{

	public function createRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('StaticPages');

		$router->add($this->createFrontRouter());

		return $router;
	}

	private function createFrontRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('Front');

		$router->addRoute('', 'Homepage:default');
		$router->addRoute('/tutorial', 'Tutorial:default');

		return $router;
	}

}
