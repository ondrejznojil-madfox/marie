<?php declare(strict_types = 1);

namespace App\Util;

class Arrays
{

	public static function find(iterable $array, callable $callback)
	{
		foreach ($array as $value) {
			if (\call_user_func($callback, $value)) {
				return $value;
			}
		}

		return NULL;
	}


	public static function findKey(array $array, callable $callback)
	{
		foreach ($array as $key => $value) {
			if (\call_user_func($callback, $value)) {
				return $key;
			}
		}

		return NULL;
	}


}
