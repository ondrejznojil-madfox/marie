<?php declare(strict_types = 1);

namespace App\LangModule\AdminModule\Grid;

class GridFactory
{

	public function __construct(
		private \App\CoreModule\AdminModule\Grid\GridFactory $gridFactory,
		private \App\LangModule\Model\LangRepository $langRepository
	)
	{
	}

	public function create(): \Mdfx\Datagrid\DataGrid
	{
		$grid = $this->gridFactory->create();

		$grid->setDataSource($this->langRepository->getGridQuery());
		$grid->addColumnText('id', '#');

		$grid->addColumnText('uid', 'UID', 'uid')
			->setSortable()
			->setFilterText()
		;

		$grid->addColumnText('value', 'Hodnota', 'translation.content')
			->setFilterText()
		;

		$grid->addEditAction('edit', 'Editovat', ':Lang:Admin:Edit:default');
		$grid->addDeleteAction('delete', 'Smazat', ':Lang:Admin:Grid:delete');

		return $grid;
	}

}
