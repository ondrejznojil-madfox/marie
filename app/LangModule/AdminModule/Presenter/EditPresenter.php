<?php declare(strict_types = 1);

namespace App\LangModule\AdminModule\Presenter;

class EditPresenter extends \App\CoreModule\Presenter\BaseAdminPresenter
{

	private ?\App\LangModule\Model\Entity\Lang $lang = NULL;


	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseAdminPresenterFacade $baseAdminPresenterFacade,
		private \App\LangModule\Model\LangRepository $langRepository,
		private \App\LangModule\AdminModule\EditForm\FormFactory $formFactory,
		private \App\LangModule\AdminModule\EditForm\DataSaver $dataSaver,
	)
	{
		parent::__construct($baseAdminPresenterFacade);
	}


	public function actionDefault(?int $id): void
	{
		$this->lang = $id ? $this->langRepository->fetch($id) : NULL;
	}


	public function renderDefault(?int $id): void
	{
		$lastUpdateDate = $this->lang ? $this->lang->getUpdatedAt() : NULL;

		$this->getTemplate()->setParameters([
			'id' => $id,
			'langName' => NULL,
			'lastUpdateDate' => $lastUpdateDate,
		]);
	}


	protected function createComponentForm(): \Nette\Application\UI\Form
	{
		$form = $this->formFactory->create($this->lang);

		$form->onSuccess[] = function (
			\Nette\Forms\Form $form,
			\App\LangModule\AdminModule\EditForm\FormData $formData
		): void {
			try {
				$newLang = $this->dataSaver->save($formData, $this->lang);
			} catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $exception) {
				$this->flashMessage('Toto UID je již použito u jiného langu.', 'error');

				return;
			}

			/** @var \Nette\Forms\Controls\SubmitButton $submitNewButton */
			$submitNewButton = $form->getComponent('submitNew');

			$this->flashMessage("admin.common.successMessage", 'ok');

			$this->redirect(':Lang:Admin:Edit:', [
				'id' => $submitNewButton->isSubmittedBy() ? NULL : $newLang->getId(),
			]);
		};

		$form->onError[] = function (\Nette\Forms\Form $form): void {
			foreach ($form->getErrors() as $error) {
				$this->flashMessage($error, 'error');
			}
		};

		return $form;
	}

}
