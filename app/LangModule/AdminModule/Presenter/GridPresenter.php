<?php declare(strict_types = 1);

namespace App\LangModule\AdminModule\Presenter;

class GridPresenter extends \App\CoreModule\Presenter\BaseAdminPresenter
{

	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseAdminPresenterFacade $baseAdminPresenterFacade,
		private \App\LangModule\AdminModule\Grid\GridFactory $gridFactory,
		private \App\LangModule\Model\LangRepository $langRepository,
		private \App\CoreModule\Service\UndoMessageGenerator $undoMessageGenerator,
	)
	{
		parent::__construct(
			$baseAdminPresenterFacade
		);
	}

	protected function createComponentGrid(): \Mdfx\Datagrid\DataGrid
	{
		return $this->gridFactory->create();
	}


	public function actionDelete(int $id): void
	{
		$lang = $this->langRepository->fetch($id);

		if ( ! $lang instanceof \App\LangModule\Model\Entity\Lang) {
			$this->redirect('default');
		}

		$this->langRepository->remove($lang);

		$message = $this->undoMessageGenerator->generate($this->link('undoRemove', [
			'id' => $id,
		]));

		$this->flashMessage($message, 'info');
		$this->redirect('default');
	}

}
