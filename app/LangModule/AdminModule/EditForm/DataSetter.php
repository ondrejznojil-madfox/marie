<?php declare(strict_types=1);

namespace App\LangModule\AdminModule\EditForm;

final class DataSetter
{

	public function set(\Nette\Forms\Form $form, \App\LangModule\Model\Entity\Lang $lang): void
	{
		$data = [
			'uid' => $lang->getUid(),
		];

		foreach (\App\LangModule\Locale\LocaleProvider::LOCALE_LIST as $locale) {
			$data[$locale] = [
				'content' => $lang->getTranslation($locale)->getContent(),
			];
		}

		$form->setDefaults($data);
	}

}
