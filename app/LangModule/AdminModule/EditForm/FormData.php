<?php declare(strict_types=1);

namespace App\LangModule\AdminModule\EditForm;

final class FormData
{

	public string $uid;

	public WebFormData $cs;
	public WebFormData $en;
	public WebFormData $de;
	public WebFormData $fi;
	public WebFormData $sv;
	public WebFormData $et;

	public function getWebData(string $locale): WebFormData
	{
		return $this->$locale;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getUid(): string
	{
		return $this->uid;
	}

}
