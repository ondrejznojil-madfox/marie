<?php declare(strict_types=1);

namespace App\LangModule\AdminModule\EditForm;

final class WebFormData
{
	
	public string $content;

	public function getContent(): string
	{
		return $this->content;
	}
}
