<?php declare(strict_types=1);

namespace App\LangModule\AdminModule\EditForm;

final class DataSaver
{

	public function __construct(
		private \App\LangModule\Model\LangRepository $langRepository,
		private \App\LangModule\Translator\TranslationsProvider $translationsProvider
	)
	{
	}

	public function save(
		FormData $formData,
		?\App\LangModule\Model\Entity\Lang $lang,
	): \App\LangModule\Model\Entity\Lang
	{

		if ($lang instanceof \App\LangModule\Model\Entity\Lang) {
			$lang->setUid($formData->getUid());
		} else {
			$lang = new \App\LangModule\Model\Entity\Lang($formData->getUid());
		}

		foreach (\App\LangModule\Locale\LocaleProvider::LOCALE_LIST as $locale) {
			$translationFormData = $formData->getWebData($locale);
			$translationEntity = $lang->getTranslation($locale);

			$translationEntity->setContent($translationFormData->getContent());
		}

		$this->langRepository->persist($lang);
		$lang->mergeNewTranslations();
		$this->langRepository->flush();

		foreach (\App\LangModule\Locale\LocaleProvider::LOCALE_LIST as $locale) {
			$this->translationsProvider->invalidateCache($locale);
		}

		return $lang;
	}

}
