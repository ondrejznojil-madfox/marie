<?php declare(strict_types=1);

namespace App\LangModule\AdminModule\EditForm;

final class FormFactory
{

	private \App\CoreModule\Service\Form\GeneralFormFactory $generalFormFactory;
	private DataSetter $dataSetter;

	public function __construct(
		\App\CoreModule\Service\Form\GeneralFormFactory $generalFormFactory,
		DataSetter $dataSetter
	)
	{
		$this->generalFormFactory = $generalFormFactory;
		$this->dataSetter = $dataSetter;
	}

	public function create(?\App\LangModule\Model\Entity\Lang $lang): \Nette\Application\UI\Form
	{
		$form = $this->generalFormFactory->create();
		$form->setMappedType(FormData::class);

		$form->addText('uid', 'UID')
			->setRequired('Toto pole je vyžadováno.')
		;

		foreach (\App\LangModule\Locale\LocaleProvider::LOCALE_LIST as $locale) {
			$form->addComponent($this->createWebContainer(), $locale);
		}

		$form->addSubmit('submit');

		$form->addSubmit('submitNew');

		if ($lang instanceof \App\LangModule\Model\Entity\Lang) {
			$this->dataSetter->set($form, $lang);
		}

		return $form;
	}


	private function createWebContainer(): \Nette\Forms\Container
	{
		$container = new \Nette\Forms\Container();
		$container->setMappedType(WebFormData::class);

		$container->addTextArea('content', 'Text překladu');

		return $container;
	}


}
