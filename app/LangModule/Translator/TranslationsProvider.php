<?php declare(strict_types = 1);

namespace App\LangModule\Translator;

class TranslationsProvider implements \App\LangModule\Translator\Contract\TranslationsProviderInterface
{

	private const BASE_CACHE_KEY = 'translations.';

	/**
	 * @var array<string, array<string, string>>
	 */
	private array $translations = [];


	private \Nette\Caching\Cache $cache;

	public function __construct(
		private \App\LangModule\Model\LangRepository $textRepository,
		\Nette\Caching\IStorage $storage
	)
	{
		$this->cache = new \Nette\Caching\Cache($storage);
	}

	/**
	 * @return array<string, string>
	 */
	public function getTranslations(string $locale): array
	{
		if (isset($this->translations[$locale])) {
			return $this->translations[$locale];
		}

		return $this->cache->load($this->getCacheKey($locale), fn (): array => $this->loadTranslations($locale));
	}


	public function invalidateCache(string $locale): void
	{
		$this->cache->remove($this->getCacheKey($locale));
		$this->translations = [];
	}


	private function loadTranslations(string $locale): array
	{
		if ( ! isset($this->translations[$locale])) {
			$this->translations[$locale] = $this->textRepository->getAllByLocale($locale);
		}

		return $this->translations[$locale];
	}


	private function getCacheKey(string $locale): string
	{
		return self::BASE_CACHE_KEY . $locale;
	}

}
