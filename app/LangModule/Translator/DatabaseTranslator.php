<?php declare(strict_types=1);

namespace App\TextModule;

class DatabaseTranslator implements \Nette\Localization\Translator
{

	public function __construct(
		private \App\LangModule\Model\LangRepository $textRepository,
		private \App\LangModule\Locale\Contract\LocaleResolverInterface $localeResolver,
		private \App\LangModule\Translator\Contract\TranslationsProviderInterface $translationsProvider,
	) {}


	public function translate($message, ...$parameters): string
	{
		if ($message === NULL) {
			return '';
		}

		return $this->translateByLocale($message, $this->localeResolver->resolve());
	}


	public function translateByLocale(string $message, string $locale): string
	{
		$result = $this->translationsProvider->getTranslations($locale)[$message] ?? '';

		return $result ?: $message;
	}

}
