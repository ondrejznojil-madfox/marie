<?php declare(strict_types=1);

namespace App\LangModule\Translator\Contract;

interface TranslationsProviderInterface
{

	/**
	 * @return array<string, string>
	 */
	public function getTranslations(string $locale): array;

}
