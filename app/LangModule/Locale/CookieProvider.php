<?php declare(strict_types = 1);

namespace App\LangModule\Locale;

class CookieProvider
{

	private const COOKIE_NAME = 'changeLocale';

	public function __construct(
		private \Nette\Http\Request $request,
		private \Nette\Http\Response $response,
	)
	{
	}


	public function setCookie(): void
	{
		$this->response->setCookie(self::COOKIE_NAME, '1', NULL);
	}


	public function isCookieSet(): bool
	{
		return (bool) $this->request->getCookie(self::COOKIE_NAME);
	}

}
