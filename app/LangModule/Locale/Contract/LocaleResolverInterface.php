<?php declare(strict_types=1);

namespace App\LangModule\Locale\Contract;

interface LocaleResolverInterface
{

	public function resolve(): string;

}
