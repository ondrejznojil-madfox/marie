<?php declare(strict_types=1);

namespace App\LangModule\Locale;

class BrowserLocaleResolver
{

	public function __construct(
		private \Nette\Http\Request $request,
		private CookieProvider $cookieProvider,
		private LocaleProvider $localeProvider,
	)
	{
	}

	public function resolve(): ?string
	{
		if ($this->cookieProvider->isCookieSet()) {
			return NULL;
		}

		return $this->request->detectLanguage($this->localeProvider->getActiveLocales());
	}

}
