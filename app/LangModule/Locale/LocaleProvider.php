<?php declare(strict_types=1);

namespace App\LangModule\Locale;

class LocaleProvider
{

	public const DEFAULT_LOCALE = 'cs';

	public const LOCALE_CS = 'cs';
	public const LOCALE_EN = 'en';

	public const LOCALE_LIST = [
		self::LOCALE_EN,
		self::LOCALE_CS,
	];

	public const LOCALE_NAMES = [
		self::LOCALE_CS => 'Čeština',
		self::LOCALE_EN => 'Angličtina',
	];

	/** @var array<string> */
	private array $activeLocales;


	public function __construct(
		array $activeLocales,
	)
	{
		$this->activeLocales = $this->formatLocales($activeLocales);
	}

	/**
	 * @return array<string>
	 */
	private function formatLocales(array $locales): array
	{
		return \array_filter(
			$locales,
			static fn (string $locale) => \array_search($locale, self::LOCALE_LIST, TRUE) !== FALSE,
		);
	}


	public static function getLocaleName(string $locale): string
	{
		return self::LOCALE_NAMES[$locale];
	}


	/**
	 * @return array<string>
	 */
	public function getActiveLocales(): array
	{
		return $this->activeLocales;
	}

}
