<?php declare(strict_types=1);

namespace App\LangModule\Locale;

class LocaleToCountryCodeMapper
{

	private const COUNTRY_CODE_CZECH = 'cz';
	private const COUNTRY_CODE_USA = 'us';
	private const COUNTRY_CODE_GERMANY = 'de';
	private const COUNTRY_CODE_FINLAND = 'fi';
	private const COUNTRY_CODE_SWEDEN = 'sv';
	private const COUNTRY_CODE_ESTONIA = 'ee';

	private const COUNTRY_CODE_LIST = [
		LocaleProvider::LOCALE_EN => self::COUNTRY_CODE_USA,
		LocaleProvider::LOCALE_CS => self::COUNTRY_CODE_CZECH,
		LocaleProvider::LOCALE_DE => self::COUNTRY_CODE_GERMANY,
		LocaleProvider::LOCALE_FI => self::COUNTRY_CODE_FINLAND,
		LocaleProvider::LOCALE_SV => self::COUNTRY_CODE_SWEDEN,
		LocaleProvider::LOCALE_ET => self::COUNTRY_CODE_ESTONIA,
	];


	public static function getCountryCode(string $locale): string
	{
		return self::COUNTRY_CODE_LIST[$locale] ?? self::COUNTRY_CODE_CZECH;
	}

}
