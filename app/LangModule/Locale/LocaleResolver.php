<?php declare(strict_types=1);

namespace App\LangModule\Locale;

class LocaleResolver implements \App\LangModule\Locale\Contract\LocaleResolverInterface
{

	public function __construct(
		private \Nette\Http\Request $request,
		private LocaleProvider $localeProvider,
	)
	{
	}

	public function resolve(): string
	{
		$locale = \substr($this->request->getUrl()->getPath(), 1, 2);

		if ( ! \in_array($locale, $this->localeProvider->getActiveLocales(), TRUE)) {
			return LocaleProvider::DEFAULT_LOCALE;
		}

		return $locale;
	}

}
