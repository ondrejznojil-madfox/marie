<?php declare(strict_types = 1);

namespace App\LangModule\Model\Entity;

class Lang extends \Mdfx\Doctrine\Model\Entity\BaseEntity
	implements \Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface
{

	use \Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

	protected string $uid;

	public function __construct(
		string $uid,
	)
	{
		parent::__construct();

		$this->uid = $uid;
	}

	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('uid', 'string')
			->unique()
			->build()
		;
	}

	public function getTranslation(string $locale = \App\LangModule\Locale\LocaleProvider::DEFAULT_LOCALE): LangTranslation
	{
		/** @var LangTranslation $translation */
		$translation = $this->translate($locale, FALSE);

		return $translation;
	}

	public function getUid(): string
	{
		return $this->uid;
	}

	public function setUid(string $uid): void
	{
		$this->uid = $uid;
	}

}
