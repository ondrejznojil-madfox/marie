<?php declare(strict_types = 1);

namespace App\LangModule\Model\Entity;

class LangTranslation extends \Mdfx\Doctrine\Model\Entity\BaseEntity
	implements \Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface
{

	use \Knp\DoctrineBehaviors\Model\Translatable\TranslationTrait;

	protected ?string $content;

	public function __construct()
	{
		parent::__construct();

		$this->content = NULL;
	}

	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('content', 'text')
			->nullable()
			->build()
		;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function setContent(?string $content): void
	{
		$this->content = $content;
	}

}
