<?php declare(strict_types = 1);

namespace App\LangModule\Model;

class LangRepository extends \Mdfx\Doctrine\Model\Repository\BaseRepository
{

	protected static ?string $entityClassName = \App\LangModule\Model\Entity\Lang::class;

	public function fetch(int $id): ?\App\LangModule\Model\Entity\Lang
	{
		/** @var \App\LangModule\Model\Entity\Lang|NULL $entity */
		$entity = parent::fetch($id);
		
		return $entity;
	}

	public function getGridQuery(
		?string $locale = \App\LangModule\Locale\LocaleProvider::DEFAULT_LOCALE,
	): \Doctrine\ORM\QueryBuilder
	{
		$b = $this->em->createQueryBuilder();

		$b->select('t, translation')
			->from(\App\LangModule\Model\Entity\Lang::class, 't')
			->leftJoin('t.translations', 'translation', \Doctrine\ORM\Query\Expr\Join::WITH, "translation.locale = '$locale'")
			->where('t.isDeleted = :isDeleted')
			->setParameter('isDeleted', FALSE)
		;

		return $b;
	}


	public function getAllByLocale(string $locale): array
	{
		$b = $this->em->createQueryBuilder();

		$b->select('t.uid, translation.content')
			->from(\App\LangModule\Model\Entity\Lang::class, 't')
			->leftJoin('t.translations', 'translation', \Doctrine\ORM\Query\Expr\Join::WITH, "translation.locale = '$locale'")
			->where('t.isDeleted = :isDeleted')
			->setParameter('isDeleted', FALSE)
		;

		$result = $b->getQuery()->getArrayResult();

		$langs = [];
		foreach ($result as $lang) {
			$langs[$lang['uid']] = $lang['content'];
		}

		return $langs;
	}


	public function getByUid(string $uid, ?string $locale = NULL): ?\App\LangModule\Model\Entity\Lang
	{
		$b = $this->em->createQueryBuilder();

		$b->select('t')
			->from(\App\LangModule\Model\Entity\Lang::class, 't')
			->where('t.isDeleted = :isDeleted')
			->andWhere('t.uid = :uid')
			->setParameter('uid', $uid)
			->setParameter('isDeleted', FALSE)
			->setMaxResults(1)
		;

		if ($locale) {
			$b
				->join('t.translations', 'at')
				->andWhere('at.locale = :locale')
				->setParameter('locale', $locale)
			;

		}

		return $b->getQuery()->getOneOrNullResult();
	}

	public function delete(\App\LangModule\Model\Entity\Lang $lang): void
	{
		$b = $this->em->createQueryBuilder();

		$b->delete()
			->from(\App\LangModule\Model\Entity\Lang::class, 'l')
			->where('l.id = :id')
			->setParameter('id', $lang->getId())
		;

		$b->getQuery()->execute();
	}

}
