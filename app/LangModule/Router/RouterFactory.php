<?php declare(strict_types = 1);

namespace App\LangModule\Router;

final class RouterFactory
{

	public function createRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('Lang');

		$router->add($this->createAdminRouter());

		return $router;
	}

	private function createAdminRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('Admin');

		$router->addRoute('[<locale=en en|cs>/]admin/translations', 'Grid:default');
		$router->addRoute('[<locale=en en|cs>/]admin/translations/edit[/<id>]', 'Edit:default');
		$router->addRoute('[<locale=en en|cs>/]admin/translations/delete[/<id>]', 'Grid:delete');

		return $router;
	}

}
