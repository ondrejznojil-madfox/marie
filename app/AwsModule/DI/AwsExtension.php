<?php declare(strict_types = 1);

namespace Mdfx\Doctrine\DI;


class AwsExtension extends \Nette\DI\CompilerExtension
{

	public function loadConfiguration()
	{
		$config = (array) $this->getConfig();

		$builder = $this->getContainerBuilder();

		$arguments = (array) $config['s3'];
		$arguments['credentials'] = new \Aws\Credentials\Credentials($config['key'], $config['secret']);

		$builder->addDefinition($this->prefix('s3Client'))
			->setFactory(\Aws\S3\S3Client::class)
			->setArguments([
				$arguments,
			])
		;

		$builder->addDefinition($this->prefix('cloudFront'))
			->setFactory(\App\AwsModule\Service\CloudFront::class)
			->setArguments([
				'cloudFrontUri' => $config['cloudFrontUri'],
			])
		;

		$builder->addDefinition($this->prefix('mediaUploader'))
			->setFactory(\App\AwsModule\Service\AwsMediaUploader::class)
			->setArguments([
				'bucket' => $arguments['bucket'],
			])
		;
	}


	public function getConfigSchema(): \Nette\Schema\Schema
	{
		return \Nette\Schema\Expect::structure([
			'key' => \Nette\Schema\Expect::string()->required(),
			'secret' => \Nette\Schema\Expect::string()->required(),
			'cloudFrontUri' => \Nette\Schema\Expect::string()->required(),
			's3' => \Nette\Schema\Expect::structure([
				'version' => \Nette\Schema\Expect::string('latest'),
				'region' => \Nette\Schema\Expect::string('eu-central-1'),
				'bucket' => \Nette\Schema\Expect::string('mdfx-ascm'),
			]),
		]);
	}

}
