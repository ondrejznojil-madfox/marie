<?php declare(strict_types = 1);

namespace App\AwsModule\Service;


class AwsMediaUploader implements \App\MediaModule\Service\MediaUploader
{

	private string $bucket;

	/**
	 * @var \Aws\S3\S3Client
	 */
	private $client;

	/**
	 * @var \App\MediaModule\Service\FileNamingStrategy
	 */
	private $fileNamingStrategy;

	/**
	 * @var \App\AwsModule\Service\CloudFront
	 */
	private $cloudFront;


	public function __construct(
		string $bucket,
		\Aws\S3\S3Client $client,
		\App\MediaModule\Service\FileNamingStrategy $fileNamingStrategy,
		CloudFront $cloudFront
	)
	{
		$this->client = $client;
		$this->fileNamingStrategy = $fileNamingStrategy;
		$this->cloudFront = $cloudFront;
		$this->bucket = $bucket;
	}

	public function upload(string $data, string $filename): \App\MediaModule\DTO\UploadResponse
	{
		$key = $this->fileNamingStrategy->getName($filename);

		$response = $this->client->putObject([
			'Bucket' => $this->bucket,
			'Key'    => $key,
			'Body'   => $data,
			'ACL'    => 'public-read',
		]);

		if ($response->get('@metadata')['statusCode'] !== 200) {
			throw new \Mdfx\Doctrine\Exception\AwsException();
		}

		return new \App\MediaModule\DTO\UploadResponse(
			$this->cloudFront->buildCloudFrontUri($key),
			$key
		);
	}


	public function delete(\App\ImageModule\Model\Entity\Image $image): void
	{
		$this->client->deleteObject([
			'Bucket' => $this->bucket,
			'Key'    => $image->getMediaKey(),
		]);
	}

}
