<?php declare(strict_types = 1);

namespace App\AwsModule\Service;


class CloudFront
{

	private string $cloudFrontUri;


	public function __construct(
		string $cloudFrontUri
	)
	{
		$this->cloudFrontUri = $cloudFrontUri;
	}


	public function buildCloudFrontUri(string $key): string
	{
		return $this->cloudFrontUri . "/$key";
	}

}
