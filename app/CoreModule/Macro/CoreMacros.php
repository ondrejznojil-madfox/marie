<?php declare(strict_types = 1);

namespace App\CoreModule\Macro;


class CoreMacros extends \Latte\Macros\MacroSet
{


	public static function install(\Latte\Compiler $compiler): void
	{
		$set = new static($compiler);
		$set->addMacro('icon', [$set, 'iconAdmin']);
		$set->addMacro('iconFront', [$set, 'iconFront']);
		$set->addMacro('assetLink', [$set, 'assetLink']);
	}


	public function iconAdmin(\Latte\MacroNode $node, \Latte\PhpWriter $writer): string
	{
		return $writer->write('echo \App\CoreModule\Macro\CoreMacros::iconAdminHelper(%escape(%node.word), $basePath, $envPath, $assetsVersion)');
	}


	public static function iconAdminHelper(string $iconName, string $basePath, string $envPath, string $assetsVersion): string
	{
		$url = $basePath . $envPath . "/icons/admin.svg?v=$assetsVersion#" . $iconName;

		return '
		<svg class="o-icon" data-icon="' . $iconName . '">
			<use xlink:href="' . $url. '"></use>
		</svg>
			';
	}


	public function iconFront(\Latte\MacroNode $node, \Latte\PhpWriter $writer): string
	{
		return $writer->write('echo \App\CoreModule\Macro\CoreMacros::iconFrontHelper(%escape(%node.word), $basePath, $envPath, $assetsVersion)');
	}


	public static function iconFrontHelper(string $iconName, string $basePath, string $envPath, string $assetsVersion): string
	{
		$url = $basePath . $envPath . "/icons/front.svg?v=$assetsVersion#" . $iconName;

		return '
		<svg class="o-icon" data-icon="' . $iconName . '">
			<use xlink:href="' . $url. '"></use>
		</svg>
			';
	}

	public function assetLink(\Latte\MacroNode $node, \Latte\PhpWriter $writer): string
	{
		return $writer->write('echo \App\CoreModule\Macro\CoreMacros::assetLinkHelper(%escape(%node.word), $basePath, $envPath)');
	}


	public static function assetLinkHelper(string $path, string $basePath, string $envPath): string
	{
		return $basePath . $envPath . $path;
	}

}
