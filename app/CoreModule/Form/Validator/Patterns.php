<?php declare(strict_types=1);

namespace App\CoreModule\Form\Validator;

class Patterns
{

	public const PHONE_PATTERN = '^(\+420)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$';
	public const ZIP_PATTERN = '^\d{3} ?\d{2}$';
	public const DIC_PATTERN = '(c|C)(z|Z)(\s*\d\s*){8,10}';
	public const ICO_PATTERN = '^\d+$';
	public const STREET = '^.*\d+$';

}
