<?php declare(strict_types = 1);

namespace App\CoreModule\Service\Form;


class GeneralFormFactory
{

	public function create(): \Nette\Application\UI\Form
	{
		return new \Nette\Application\UI\Form();
	}

}
