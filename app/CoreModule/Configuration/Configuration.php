<?php declare(strict_types = 1);

namespace App\CoreModule\Configuration;

class Configuration
{
	private const DEV = 'dev';
	private const STAGING = 'staging';
	private const PRODUCTION = 'production';

	private const ALLOWED_ENV = [
		self::DEV,
		self::STAGING,
		self::PRODUCTION,
	];

	private string $env;
	private string $assetsVersion;

	public function __construct(
		string $env,
		string $assetsVersion
	)
	{
		if ( ! \in_array($env, self::ALLOWED_ENV, TRUE)) {
			throw new \UnexpectedValueException('Environment is out of allowed set of options.');
		}

		$this->env = $env;
		$this->assetsVersion = $assetsVersion;
	}

	public function isDev(): bool
	{
		return $this->env === self::DEV;
	}

	public function isProduction(): bool
	{
		return $this->env === self::PRODUCTION;
	}

	public function isStaging(): bool
	{
		return $this->env === self::STAGING;
	}

	public function getBasePath(): string
	{
		return $this->isDev() ? '/assets-dev' : '/assets';
	}

	public function getAssetsVersion(): string
	{
		return $this->assetsVersion;
	}
}
