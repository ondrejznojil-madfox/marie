<?php declare(strict_types = 1);

namespace App\CoreModule\Template\Filter;

class RomanNumeralsFilter
{

	private static array $map = [
		1 => 'I',
		2 => 'II',
		3 => 'III',
		4 => 'IV',
		5 => 'V',
	];

	public static function invoke(string $input): string
	{
		return self::$map[(int) $input];
	}


}
