<?php declare(strict_types = 1);

namespace App\CoreModule\Template\Filter;

class AllergensFilter
{

	public static function invoke(\App\MenuModule\Model\Entity\AllergenicMealInterface $meal): string
	{
		$allergens = \Mdfx\Doctrine\Util\ArrayUtil::getCollectionIds($meal->getAllergens());

		if ( ! $allergens) {
			return '';
		}

		$formatted = \implode(', ', $allergens);

		return "($formatted)";
	}

}
