<?php declare(strict_types = 1);

namespace App\CoreModule\Template\Filter;

class PriceFilter
{

	public static function invoke(string $input): string
	{
		return \number_format((float) $input, 0, '.', ' ') . html_entity_decode('&nbsp;') . 'Kč';
	}

}
