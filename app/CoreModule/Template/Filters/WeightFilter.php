<?php declare(strict_types = 1);

namespace App\CoreModule\Template\Filter;

class WeightFilter
{

	public static function invoke(string $input): string
	{
		return $input . html_entity_decode('&nbsp;') . 'g';
	}

}
