<?php declare(strict_types = 1);

namespace App\CoreModule\Template\Filter;

class WeekDayFilter
{

	public static function invoke(string $input): string
	{
		return \App\MenuModule\WeekHelper\DayNames::getDayName((int) $input);
	}

}
