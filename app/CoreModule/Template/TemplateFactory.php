<?php declare(strict_types = 1);

namespace App\CoreModule\Template;

class TemplateFactory implements \Nette\Application\UI\ITemplateFactory
{

	public function __construct(
		private \Nette\Application\UI\ITemplateFactory $templateFactory,
		private \App\CoreModule\Configuration\Configuration $configuration,
		private \Mdfx\PictureGenerator\Generator\PictureGeneratorFacade $pictureGenerator,
		private \App\ImageModule\Service\MediaGetter $mediaGetter,
	) {}

	public function createTemplate(?\Nette\Application\UI\Control $control = NULL): \Nette\Application\UI\ITemplate
	{
		/** @var \Nette\Bridges\ApplicationLatte\Template $template */
		$template = $this->templateFactory->createTemplate($control);

		$template->setParameters([
			'envPath' => $this->configuration->getBasePath(),
			'assetsVersion' => $this->configuration->getAssetsVersion(),
			'pictureGenerator' => $this->pictureGenerator,
			'mediaGetter' => $this->mediaGetter,
		]);

		$this->registerFilters($template);

		return $template;
	}

	private function registerFilters(\Nette\Bridges\ApplicationLatte\Template $template): void
	{
		$template->addFilter(
			'romanNumerals',
			static fn (string $input): string => \App\CoreModule\Template\Filter\RomanNumeralsFilter::invoke($input),
		);

		$template->addFilter(
			'price',
			static fn (string $input): string => \App\CoreModule\Template\Filter\PriceFilter::invoke($input),
		);

		$template->addFilter(
			'weekDay',
			static fn (string $input): string => \App\CoreModule\Template\Filter\WeekDayFilter::invoke($input),
		);

		$template->addFilter(
			'weight',
			static fn (string $input): string => \App\CoreModule\Template\Filter\WeightFilter::invoke($input),
		);

		$template->addFilter(
			'allergens',
			static fn (\App\MenuModule\Model\Entity\AllergenicMealInterface $meal): string => \App\CoreModule\Template\Filter\AllergensFilter::invoke($meal),
		);
	}

}
