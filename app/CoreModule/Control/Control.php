<?php declare(strict_types = 1);

namespace App\CoreModule\Control;

/** @method \Nette\Bridges\ApplicationLatte\Template getTemplate() */
class Control extends \Nette\Application\UI\Control
{

	protected static string $fileTemplate  = __DIR__ . '/default.latte';

	private array $onError = [];

	private array $onSuccess = [];

	protected \App\CoreModule\Configuration\Configuration $configuration;

	public function __construct(
		\App\CoreModule\Configuration\Configuration $configuration
	)
	{
		$this->configuration = $configuration;
	}

	public function render(): void
	{
		$parameters = $this->setTemplateParameters();

		$this->getTemplate()->setParameters($parameters);

		$this->getTemplate()->render(static::$fileTemplate);
	}


	protected function setTemplateParameters(): array
	{
		return [
			'envPath' => $this->configuration->getBasePath(),
		];
	}


	protected function dispatchOnSuccess(\Nette\Forms\Form $form, object $data): void
	{
		foreach ($this->onSuccess as $onSuccess) {
			\call_user_func($onSuccess, $form, $data);
		}
	}


	public function addOnSuccess(callable $callback): void
	{
		$this->onSuccess[] = $callback;
	}


	protected function dispatchOnError(array $errors): void
	{
		foreach ($this->onError as $onError) {
			\call_user_func($onError, $errors);
		}
	}


	public function addOnError(callable $callback): void
	{
		$this->onError[] = $callback;
	}

}
