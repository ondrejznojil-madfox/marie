<?php declare(strict_types=1);

namespace App\CoreModule\Service;

class VocativeParser
{

	private ?\Haltuf\Genderer\Genderer $parser = NULL;

	public function getVocative(string $name): string
	{
		return $this->getParser()->getVocative($name);
	}

	private function getParser(): \Haltuf\Genderer\Genderer
	{
		if ($this->parser === NULL) {
			$this->parser = new \Haltuf\Genderer\Genderer();
		}

		return $this->parser;
	}

}
