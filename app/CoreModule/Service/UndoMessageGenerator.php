<?php declare(strict_types = 1);

namespace App\CoreModule\Service;


class UndoMessageGenerator
{
	private const SUCCESS_MESSAGE = 'Položka smazána.';
	private const UNDO_MESSAGE = 'Vrátit zpět.';

	public function generate(string $link, ?string $successMessage = NULL, ?string $undoMessage = NULL): string
	{
		$successMessage ??= self::SUCCESS_MESSAGE;
		$undoMessage ??= self::UNDO_MESSAGE;

		$el = \Nette\Utils\Html::el('a', [
			'class' => [ 'najdiSiToVPhpStormADoplnCoPotrebujes' ],
		]);

		$el->data('target-loader', '.c-content');
		$el->href($link);
		$el->setText($undoMessage);

		return "$successMessage $el";
	}

}
