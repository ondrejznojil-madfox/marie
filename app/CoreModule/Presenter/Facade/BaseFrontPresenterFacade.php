<?php declare(strict_types = 1);

namespace App\CoreModule\Presenter\Facade;

final class BaseFrontPresenterFacade
{

	private \App\LayoutModule\FrontModule\UI\HeadControl\HeadControlFactory $headControlFactory;
	private \App\LayoutModule\FrontModule\UI\HeaderControl\HeaderControlFactory $headerControlFactory;
	private \App\LayoutModule\FrontModule\UI\FooterControl\FooterControlFactory $footerControlFactory;
	private \App\CoreModule\Configuration\Configuration $configuration;

	public function __construct(
		\App\LayoutModule\FrontModule\UI\HeadControl\HeadControlFactory $headControlFactory,
		\App\LayoutModule\FrontModule\UI\HeaderControl\HeaderControlFactory $headerControlFactory,
		\App\LayoutModule\FrontModule\UI\FooterControl\FooterControlFactory $footerControlFactory,
		\App\CoreModule\Configuration\Configuration $configuration
	)
	{
		$this->headControlFactory = $headControlFactory;
		$this->headerControlFactory = $headerControlFactory;
		$this->footerControlFactory = $footerControlFactory;
		$this->configuration = $configuration;
	}

	public function getHeadControlFactory(): \App\LayoutModule\FrontModule\UI\HeadControl\HeadControlFactory
	{
		return $this->headControlFactory;
	}

	public function getHeaderControlFactory(): \App\LayoutModule\FrontModule\UI\HeaderControl\HeaderControlFactory
	{
		return $this->headerControlFactory;
	}

	public function getFooterControlFactory(): \App\LayoutModule\FrontModule\UI\FooterControl\FooterControlFactory
	{
		return $this->footerControlFactory;
	}

	public function getConfiguration(): \App\CoreModule\Configuration\Configuration
	{
		return $this->configuration;
	}

}
