<?php declare(strict_types = 1);

namespace App\CoreModule\Presenter\Facade;

final class BaseAdminPresenterFacade
{


	private \App\LayoutModule\AdminModule\UI\FlashMessage\FlashMessageControlFactory $flashMessageControlFactory;
	private \App\CoreModule\Configuration\Configuration $configuration;

	public function __construct(
		\App\LayoutModule\AdminModule\UI\FlashMessage\FlashMessageControlFactory $flashMessageControlFactory,
		\App\CoreModule\Configuration\Configuration $configuration
	)
	{
		$this->flashMessageControlFactory = $flashMessageControlFactory;
		$this->configuration = $configuration;
	}

	public function getFlashMessageControlFactory(
	): \App\LayoutModule\AdminModule\UI\FlashMessage\FlashMessageControlFactory
	{
		return $this->flashMessageControlFactory;
	}

	public function getConfiguration(): \App\CoreModule\Configuration\Configuration
	{
		return $this->configuration;
	}


}
