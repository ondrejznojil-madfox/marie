<?php declare(strict_types = 1);

namespace App\CoreModule\Presenter;


abstract class BaseAdminPresenter extends BasePresenter
{
	private const LAYOUT_FILE = 'AdminModule/templates/@layout.latte';

	protected ?string $backlink = NULL;

	private Facade\BaseAdminPresenterFacade $baseAdminPresenterFacade;

	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseAdminPresenterFacade $baseAdminPresenterFacade
	)
	{
		parent::__construct();

		$this->baseAdminPresenterFacade = $baseAdminPresenterFacade;
	}


	protected function createComponentFlashMessage(): \App\LayoutModule\AdminModule\UI\FlashMessage\FlashMessageControl
	{
		return $this->baseAdminPresenterFacade->getFlashMessageControlFactory()->create();
	}

	public function beforeRender(): void
	{
		parent::beforeRender();

		$this->getTemplate()->setParameters([
			'envPath' => $this->baseAdminPresenterFacade->getConfiguration()->getBasePath(),
			'assetsVersion' => $this->baseAdminPresenterFacade->getConfiguration()->getAssetsVersion(),
		]);

		$this->setLayout(self::LAYOUT_BASE_DIR . self::LAYOUT_FILE);
	}

	/**
	 * @throws \Nette\Application\AbortException
 */
	public function startup(): void
	{
		parent::startup();

		if ( ! $this->isLoginPage() && ! $this->getUser()->isLoggedIn()) {
			$this->backlink = $this->storeRequest();
			$this->redirect(':User:Admin:Login:login');
		}

		if ($this->isLoginPage() && $this->getUser()->isLoggedIn()) {
			$this->redirect(':Horizon:Admin:CaseGrid:');
		}
	}


	private function isLoginPage(): bool
	{
		$request = $this->getRequest();

		return $request && $request->getPresenterName() === 'User:Admin:Login' && $this->getAction() !== 'logout';
	}

}
