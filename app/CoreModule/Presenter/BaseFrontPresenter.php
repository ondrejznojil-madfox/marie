<?php declare(strict_types = 1);

namespace App\CoreModule\Presenter;

abstract class BaseFrontPresenter extends BasePresenter
{
	private const LAYOUT_FILE = 'FrontModule/templates/@layout.latte';

	protected ?string $backlink = NULL;

	private \App\CoreModule\Presenter\Facade\BaseFrontPresenterFacade $baseFrontPresenterFacade;

	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseFrontPresenterFacade $baseFrontPresenterFacade
	)
	{
		parent::__construct();

		$this->baseFrontPresenterFacade = $baseFrontPresenterFacade;
	}

	public function beforeRender(): void
	{
		parent::beforeRender();

		$this->getTemplate()->setParameters([
			'isProduction' => $this->baseFrontPresenterFacade->getConfiguration()->isProduction(),
			'envPath' => $this->baseFrontPresenterFacade->getConfiguration()->getBasePath(),
			'assetsVersion' => $this->baseFrontPresenterFacade->getConfiguration()->getAssetsVersion(),
		]);

		$this->setLayout(self::LAYOUT_BASE_DIR . self::LAYOUT_FILE);
	}

	protected function createComponentHead(): \App\LayoutModule\FrontModule\UI\HeadControl\HeadControl
	{
		return $this->baseFrontPresenterFacade->getHeadControlFactory()->create();
	}

	protected function createComponentHeader(): \App\LayoutModule\FrontModule\UI\HeaderControl\HeaderControl
	{
		return $this->baseFrontPresenterFacade->getHeaderControlFactory()->create();
	}

	protected function createComponentFooter(): \App\LayoutModule\FrontModule\UI\FooterControl\FooterControl
	{
		return $this->baseFrontPresenterFacade->getFooterControlFactory()->create();
	}

	protected function setMetaTitle(?string $metaTitle): void
	{
		$this->getHeadControl()->setMetaTitle($metaTitle);
	}

	protected function setMetaDesc(?string $metaDesc): void
	{
		$this->getHeadControl()->setMetaDesc($metaDesc);
	}

	protected function setNoIndex(): void
	{
		$this->getHeadControl()->setNoIndex();
	}

	protected function setOgImage(?\Mdfx\PictureGenerator\Contract\ImageInterface $image): void
	{
		$this->getHeadControl()->setOgImage($image);
	}

	private function getHeadControl(): \App\LayoutModule\FrontModule\UI\HeadControl\HeadControl
	{
		/** @var \App\LayoutModule\FrontModule\UI\HeadControl\HeadControl $headControl */
		$headControl = $this->getComponent('head');

		return $headControl;
	}

}
