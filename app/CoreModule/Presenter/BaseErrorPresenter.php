<?php declare(strict_types = 1);

namespace App\CoreModule\Presenter;


final class BaseErrorPresenter implements \Nette\Application\IPresenter
{
	private \Tracy\ILogger $logger;
	private \Nette\Http\Request $httpRequest;

	public function __construct(
		\Tracy\ILogger $logger,
		\Nette\Http\Request $httpRequest
	) {
		$this->logger = $logger;
		$this->httpRequest = $httpRequest;
	}

	public function run(\Nette\Application\Request $request): \Nette\Application\IResponse
	{
		$e = $request->getParameter('exception');

		[$module, , $sep] = \Nette\Application\Helpers::splitName($request->getPresenterName());
		$isAdmin = \strpos($this->httpRequest->getUrl()->getPath(), 'admin') !== FALSE;
		$adminFrontModule = $isAdmin ? 'Admin' : 'Front';

		if ($e instanceof \Nette\Application\BadRequestException) {
			$this->logger->log("HTTP code {$e->getCode()}: {$e->getMessage()} in {$e->getFile()}:{$e->getLine()}", 'access');
			$presenterName = 'Error4xx';
		} else {
			$presenterName = 'Error';
		}

		$errorPresenter = $module . $sep . $adminFrontModule . $sep . $presenterName;

		$this->logger->log($e, \Tracy\ILogger::EXCEPTION);

		return new \Nette\Application\Responses\ForwardResponse($request->setPresenterName($errorPresenter));
	}
}
