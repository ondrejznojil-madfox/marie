<?php declare(strict_types = 1);

namespace App\CoreModule\Presenter;

/**
 * @method \Nette\Bridges\ApplicationLatte\Template getTemplate()
 */
abstract class BasePresenter extends \Nette\Application\UI\Presenter
{

	protected const LAYOUT_BASE_DIR = __DIR__ . '/../../LayoutModule/';

}
