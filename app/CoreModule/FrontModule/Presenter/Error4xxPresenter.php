<?php declare(strict_types = 1);

namespace App\CoreModule\FrontModule\Presenter;

final class Error4xxPresenter extends \App\CoreModule\Presenter\BaseFrontPresenter
{


	public function renderDefault(\Nette\Application\BadRequestException $exception): void
	{
		$file = __DIR__ . "/../templates/Error4xx/{$exception->getCode()}.latte";

		$file = \is_file($file) ? $file : __DIR__ . '/../templates/Error4xx/4xx.latte';

		$this->getTemplate()->add('code', $exception->getCode());
		$this->getTemplate()->setFile($file);
	}
}
