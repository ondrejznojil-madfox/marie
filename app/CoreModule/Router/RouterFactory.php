<?php declare(strict_types = 1);

namespace App\CoreModule\Router;

final class RouterFactory
{

	public function __construct(
		private \App\StaticPagesModule\Router\RouterFactory $staticPagesRouterFactory,
		private \App\UserModule\Router\RouterFactory $userRouterFactory,
		private \App\HorizonModule\Router\RouterFactory $horizonRouterFactory,
		private \App\LangModule\Router\RouterFactory $langRouterFactory,
	)
	{
	}

	public function createRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList();

		$router->add($this->staticPagesRouterFactory->createRouter());
		$router->add($this->userRouterFactory->createRouter());
		$router->add($this->horizonRouterFactory->createRouter());
		$router->add($this->langRouterFactory->createRouter());

		return $router;
	}

}
