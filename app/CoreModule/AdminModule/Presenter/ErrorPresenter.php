<?php declare(strict_types=1);

namespace App\CoreModule\AdminModule\Presenter;

final class ErrorPresenter implements \Nette\Application\IPresenter
{

	public function run(\Nette\Application\Request $request): \Nette\Application\IResponse
	{
		return new \Nette\Application\Responses\CallbackResponse(static function (
			\Nette\Http\IRequest $httpRequest,
			\Nette\Http\IResponse $httpResponse
		): void {
			if (preg_match('#^text/html(?:;|$)#', (string) $httpResponse->getHeader('Content-Type'))) {
				require __DIR__ . '/../templates/Error/500.phtml';
			}
		});
	}
}
