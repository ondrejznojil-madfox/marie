<?php declare(strict_types = 1);

namespace App\CoreModule\AdminModule\Grid;


class GridFactory
{

	private \Mdfx\Datagrid\GridFactory $gridFactory;

	public function __construct(
		\Mdfx\Datagrid\GridFactory $gridFactory
	)
	{
		$this->gridFactory = $gridFactory;
	}

	public function create(): \Mdfx\Datagrid\DataGrid
	{
		return $this->gridFactory->create();
	}

}
