<?php declare(strict_types = 1);

namespace App\CoreModule\AdminModule\UI\FlashMessage;


interface FlashMessageControlFactory
{
	public function create(): FlashMessageControl;
}
