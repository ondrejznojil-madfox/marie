<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\FlashMessage;


class FlashMessageControl extends \Nette\Application\UI\Control
{
	public const MSG_ERROR = 'error';
	public const MSG_INFO = 'info';
	public const MSG_SUCCESS = 'ok';

	private const FILE_TEMPLATE = __DIR__ . '/flashMessage.latte';

	/**
	 * @var array
	 */
	private $flashMessages = [];


	public function render(): void
	{
		/** @var \Nette\Bridges\ApplicationLatte\Template $template */
		$template = $this->getTemplate();

		$this->setParentFlashMessages();

		$template->setParameters([
		   'flashes' => $this->flashMessages,
		]);

		$template->render(self::FILE_TEMPLATE);
	}


	private function setParentFlashMessages(): void
	{
		/** @var \Nette\Application\UI\Control $parentComponent */
		$parentComponent = $this->getParent();

		/** @var \Nette\Bridges\ApplicationLatte\Template $parentTemplate */
		$parentTemplate = $parentComponent->getTemplate();

		$flashes = $parentTemplate->getParameters()['flashes'] ?? [];
		$flashesDirect = $parentTemplate->getParameters()['flashesDirect'] ?? NULL;

		if ($flashesDirect) {
			$flashes = array_merge($flashes, $flashesDirect);
		}

		foreach ($flashes as $flash) {
			$this->flashMessages[] = $flash;
		}
	}


	public function addFlashMessage(\stdClass $flashMessage): void
	{
		$this->flashMessages[] = $flashMessage;
	}
}
