<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\FlashMessage;


interface FlashMessageControlFactory
{
	public function create(): FlashMessageControl;
}
