<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\OfferUnavailableControl;


class OfferUnavailableControl extends \App\CoreModule\Control\Control
{

	protected static string $fileTemplate = __DIR__ . '/offerUnavailableControl.latte';

}
