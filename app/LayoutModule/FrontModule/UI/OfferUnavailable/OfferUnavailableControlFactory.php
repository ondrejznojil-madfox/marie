<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\OfferUnavailableControl;


interface OfferUnavailableControlFactory
{

	public function create(): OfferUnavailableControl;

}
