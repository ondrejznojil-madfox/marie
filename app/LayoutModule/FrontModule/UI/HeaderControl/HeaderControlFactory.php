<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\HeaderControl;


interface HeaderControlFactory
{

	public function create(
	): HeaderControl;

}
