<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\HeaderControl;

class HeaderControl extends \App\CoreModule\Control\Control
{

	protected static string $fileTemplate  = __DIR__ . '/headerControl.latte';

	private \App\UserModule\Model\Repository\UserRepository $userRepository;

	private \Nette\Security\User $user;

	private \Nette\Http\Request $request;

	public function __construct(
		\App\CoreModule\Configuration\Configuration $configuration,
		\App\UserModule\Model\Repository\UserRepository $userRepository,
		\Nette\Security\User $user,
		\Nette\Http\Request $request,
	)
	{
		parent::__construct($configuration);

		$this->userRepository = $userRepository;
		$this->user = $user;
		$this->request = $request;
	}

	public function render(array $params = []): void
	{
		/** @var \Nette\Bridges\ApplicationLatte\Template $template */
		$template = $this->getTemplate();

		/** @var \App\UserModule\Model\Entity\User|NULL $user */
		$user = $this->user->isLoggedIn() ? $this->userRepository->fetch((int) $this->user->getId()) : NULL;

		$params += [
			'backlink' => (string) $this->request->getUrl(),
		];

		$template->setParameters(\array_merge($this->setTemplateParameters(), $params));

		$template->render(static::$fileTemplate);
	}
}
