<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\FooterControl;


interface FooterControlFactory
{

	public function create(): FooterControl;

}
