<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\FooterControl;


class FooterControl extends \App\CoreModule\Control\Control
{

	protected static string $fileTemplate = __DIR__ . '/footerControl.latte';

}
