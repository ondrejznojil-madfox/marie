<?php declare(strict_types=1);

namespace App\LayoutModule\FrontModule\UI\HeadControl;

class HeadControl extends \App\CoreModule\Control\Control
{

	protected static string $fileTemplate = __DIR__ . '/headControl.latte';

	private \App\CoreModule\FrontModule\UI\StylesheetsControl\StylesheetsControlFactory $stylesheetsControlFactory;

	public function __construct(
		\App\CoreModule\Configuration\Configuration $configuration,
		\App\CoreModule\FrontModule\UI\StylesheetsControl\StylesheetsControlFactory $stylesheetsControlFactory
	)
	{
		parent::__construct($configuration);
		$this->stylesheetsControlFactory = $stylesheetsControlFactory;
	}

	protected function createComponentStylesheets(): \App\CoreModule\FrontModule\UI\StylesheetsControl\StylesheetsControl
	{
		return $this->stylesheetsControlFactory->create();
	}

	public function setTemplateParameters(): array
	{
		$this->setNoIndex();

		return [
			'isProduction' => $this->configuration->isProduction(),
            'defaultMetaTitle' => 'Výzkum',
			'defaultMetaDesc' => 'Meta description',
			'defaultOgImage' => NULL,
		];
	}

	public function setMetaTitle(?string $metaTitle): void
	{
		if ($metaTitle) {
			$this->getTemplate()->add('metaTitle', $metaTitle);
		}
	}

	public function setMetaDesc(?string $metaDesc): void
	{
		if ($metaDesc) {
			$this->getTemplate()->add('metaDesc', $metaDesc);
		}
	}

	public function setOgImage(?\Mdfx\PictureGenerator\Contract\ImageInterface $image): void
	{
		$this->getTemplate()->add('ogImage', $image);
	}

	public function setNoIndex(): void
	{
		$this->getTemplate()->add('noIndex', TRUE);
	}

}
