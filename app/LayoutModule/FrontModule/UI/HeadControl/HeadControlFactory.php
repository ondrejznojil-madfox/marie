<?php declare(strict_types = 1);

namespace App\LayoutModule\FrontModule\UI\HeadControl;


interface HeadControlFactory
{

	public function create(): HeadControl;

}
