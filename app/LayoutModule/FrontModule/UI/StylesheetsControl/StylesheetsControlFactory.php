<?php declare(strict_types=1);

namespace App\CoreModule\FrontModule\UI\StylesheetsControl;

interface StylesheetsControlFactory
{

	public function create() : StylesheetsControl;

}
