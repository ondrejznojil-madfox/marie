<?php declare(strict_types=1);

namespace App\CoreModule\FrontModule\UI\StylesheetsControl;

class StylesheetsControl extends \App\CoreModule\Control\Control
{
	protected static string $fileTemplate = __DIR__ . '/stylesheetsControl.latte';

	protected function setTemplateParameters(): array
	{
		$criticalStylesheets = [
			$this->configuration->getBasePath() . "/css/front.critical.css?v{$this->configuration->getAssetsVersion()}",
		];

		$externalStylesheets = [
			'https://fonts.googleapis.com/css2?family=Kaushan+Script&family=Raleway:wght@200;300;400;500;700;800&display=swap',
		];

		$internalStylesheets = [
			$this->configuration->getBasePath() . "/css/front.css?v{$this->configuration->getAssetsVersion()}",
		];

		return [
			'criticalStylesheets' => $criticalStylesheets,
			'externalStylesheets' => $externalStylesheets,
			'internalStylesheets' => $internalStylesheets,
		];
	}
}
