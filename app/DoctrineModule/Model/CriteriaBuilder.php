<?php declare(strict_types = 1);

namespace App\DoctrineModule\Model;


class CriteriaBuilder
{

	public static function createSortByPositionCriteria(string $alias = ''): \Doctrine\Common\Collections\Criteria
	{
		$key = $alias ? "$alias.position" : 'position';

		return \Doctrine\Common\Collections\Criteria::create()
			->orderBy([
				$key => 'ASC',
			]);
	}

	public static function createSortByDayCriteria(string $alias = ''): \Doctrine\Common\Collections\Criteria
	{
		$key = $alias ? "$alias.day" : 'day';

		return \Doctrine\Common\Collections\Criteria::create()
			->orderBy([
				$key => 'ASC',
			]);
	}

}
