<?php declare(strict_types = 1);

namespace App\UserModule\Router;


final class RouterFactory
{

	public function createRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('User');

		$router->add($this->createAdminRouter());
		$router->add($this->createFrontRouter());

		return $router;
	}

	private function createAdminRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('Admin');

		$router->addRoute('/admin', 'Login:login');
		$router->addRoute('/admin/login', 'Login:login');
		$router->addRoute('/admin/logout', 'Login:logout');

		return $router;
	}


	private function createFrontRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('Front');

		$router->addRoute('/zapomenute-heslo', 'ForgottenPassword:default');
		$router->addRoute('/obnoveni-hesla', 'ResetPassword:default');
		$router->addRoute('/obnoveni-hesla-se-nezdarilo', 'ResetPassword:incorrectToken');

		$router->addRoute('/prihlasit-se', 'Login:default');
		$router->addRoute('/odhlasit-se', 'Login:logout');
		$router->addRoute('/registrace', 'Register:default');

		$router->addRoute('/profil', 'Profile:default');

		return $router;
	}
}
