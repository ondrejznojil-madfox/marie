<?php declare(strict_types=1);

namespace App\UserModule\ForgottenPassword;

class FormFactory
{

	private \App\CoreModule\Service\Form\GeneralFormFactory $generalFormFactory;

	public function __construct(
		\App\CoreModule\Service\Form\GeneralFormFactory $generalFormFactory
	)
	{
		$this->generalFormFactory = $generalFormFactory;
	}

	public function create(): \Nette\Application\UI\Form
	{
		$form = $this->generalFormFactory->create();

		$form->setMappedType(FormData::class);

		$form->addEmail('email', 'E-mail')
			->setRequired('Pole E-mail je vyžadováno.')
			->addRule(\Nette\Forms\Form::EMAIL, 'Zadejte prosím validní e-mailovou adresu.')
		;

		$form->addSubmit('submit');

		return $form;
	}

}
