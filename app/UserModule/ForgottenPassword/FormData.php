<?php declare(strict_types = 1);

namespace App\UserModule\ForgottenPassword;


class FormData
{

	public string $email;

	public function getEmail(): string
	{
		return $this->email;
	}

}
