<?php declare(strict_types = 1);

namespace App\UserModule\AdminModule\Grid;


class GridFactory
{
	private \Mdfx\Datagrid\GridFactory $gridFactory;
	private \App\UserModule\Model\Repository\UserRepository $userRepository;

	public function __construct(
		\Mdfx\Datagrid\GridFactory $gridFactory,
		\App\UserModule\Model\Repository\UserRepository $userRepository
	)
	{
		$this->gridFactory = $gridFactory;
		$this->userRepository = $userRepository;
	}


	public function create(): \Mdfx\Datagrid\DataGrid
	{
		$grid = $this->gridFactory->create();

		$grid->setDataSource($this->userRepository->getGridQuery());
		$grid->addColumnText('id', '#');
		$grid->setDefaultSort('createdAt DESC');


		$grid->addColumnText('email', 'email', 'email')
			->setSortable()
			->setFilterText()
		;

		$grid->addColumnText('isActive', 'Zaplacené členství')
			->setSortable()
			->setRenderer(static function (\App\UserModule\Model\Entity\User $row): string {
				return $row->isActive() ? 'Ano' : 'Ne';
			})
			->setFilterSelect([
				0 => 'Ne',
				1 => 'Ano',
			])
			->setPrompt('Vybrat')
		;

		$grid->addColumnText('companyName', 'Název společnosti', 'translation.companyName')
			->setSortable()
			->setFilterText('ut.companyName')
		;

		$grid->addColumnText('firstName', 'Jméno', 'translation.firstName')
			->setSortable()
			->setFilterText('ut.firstName')
		;

		$grid->addColumnText('lastName', 'Příjmení', 'translation.lastName')
			->setSortable()
			->setFilterText('ut.lastName')
		;

		$grid->addColumnDateTime('createdAt', 'Datum registrace')
			->setFormat('d. m. Y H:i')
			->setSortable()
		;

		$grid->addEditAction('edit', 'Upravit', ':User:Admin:Edit:default');

		return $grid;
	}

}
