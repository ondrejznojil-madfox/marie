<?php declare(strict_types = 1);

namespace App\UserModule\AdminModule\UI\LoginControl;


interface LoginControlFactory
{

	public function create(): LoginControl;

}
