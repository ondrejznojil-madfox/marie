<?php declare(strict_types = 1);

namespace App\UserModule\AdminModule\UI\LoginControl;


class LoginControl extends \App\CoreModule\Control\Control
{

	protected static string $fileTemplate  = __DIR__ . '/loginControl.latte';

	private \App\CoreModule\Service\Form\GeneralFormFactory $generalFormFactory;
	private \App\LayoutModule\AdminModule\UI\FlashMessage\FlashMessageControlFactory $flashMessageControlFactory;

	public function __construct(
		\App\CoreModule\Service\Form\GeneralFormFactory $generalFormFactory,
		\App\CoreModule\Configuration\Configuration $configuration,
		\App\LayoutModule\AdminModule\UI\FlashMessage\FlashMessageControlFactory $flashMessageControlFactory
	)
	{
		parent::__construct($configuration);

		$this->generalFormFactory = $generalFormFactory;
		$this->configuration = $configuration;
		$this->flashMessageControlFactory = $flashMessageControlFactory;
	}

	protected function createComponentFlashMessage(): \App\LayoutModule\AdminModule\UI\FlashMessage\FlashMessageControl
	{
		return $this->flashMessageControlFactory->create();
	}

	public function createComponentForm(): \Nette\Application\UI\Form
	{
		$form = $this->generalFormFactory->create();

		$form->setMappedType(LoginFormData::class);

		$form->addText('email')
			->addRule(\Nette\Forms\Form::EMAIL, 'Zadejte prosím validní e-mail.');
		$form->addPassword('password');

		$form->onSuccess[] = function (\Nette\Forms\Form $form, LoginFormData $data): void {
			$this->dispatchOnSuccess($form, $data);
		};

		$form->addSubmit('submit');

		return $form;
	}

}
