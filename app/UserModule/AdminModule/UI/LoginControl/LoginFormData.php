<?php declare(strict_types = 1);

namespace App\UserModule\AdminModule\UI\LoginControl;


final class LoginFormData
{

	public string $email;

	public string $password;

	public function getEmail(): string
	{
		return $this->email;
	}

	public function getPassword(): string
	{
		return $this->password;
	}

}
