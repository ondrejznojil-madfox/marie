<?php declare(strict_types = 1);

namespace App\UserModule\AdminModule\Presenter;


class GridPresenter extends \App\CoreModule\Presenter\BaseAdminPresenter
{

	private \App\UserModule\AdminModule\Grid\GridFactory $gridFactory;


	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseAdminPresenterFacade $baseAdminPresenterFacade,
		\App\UserModule\AdminModule\Grid\GridFactory $gridFactory
	)
	{
		parent::__construct($baseAdminPresenterFacade);

		$this->gridFactory = $gridFactory;
	}


	protected function createComponentGrid(): \Mdfx\Datagrid\DataGrid
	{
		return $this->gridFactory->create();
	}

}
