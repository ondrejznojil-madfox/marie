<?php declare(strict_types = 1);

namespace App\UserModule\AdminModule\Presenter;


class LoginPresenter extends \App\CoreModule\Presenter\BaseAdminPresenter
{

	private \App\UserModule\AdminModule\UI\LoginControl\LoginControlFactory $loginControlFactory;

	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseAdminPresenterFacade $baseAdminPresenterFacade,
		\App\UserModule\AdminModule\UI\LoginControl\LoginControlFactory $loginControlFactory
	)
	{
		parent::__construct($baseAdminPresenterFacade);

		$this->loginControlFactory = $loginControlFactory;
	}

	protected function createComponentLogin(): \App\UserModule\AdminModule\UI\LoginControl\LoginControl
	{
		$control = $this->loginControlFactory->create();

		$control->addOnSuccess(function (\Nette\Forms\Form $form, \App\UserModule\AdminModule\UI\LoginControl\LoginFormData $data) use ($control): void {
			try {
				$this->getUser()->login($data->getEmail(), $data->getPassword());

				$this->redirect(':Horizon:Admin:CaseGrid:default');
			} catch (\Nette\Security\AuthenticationException $e) {
				$control->flashMessage('Nesprávné jméno, nebo heslo', 'error');
			}
		});

		return $control;
	}

	public function actionLogout(): void
	{
		$this->getUser()->logout(TRUE);

		$this->redirect('login');
	}

}
