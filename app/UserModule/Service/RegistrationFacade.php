<?php declare(strict_types=1);

namespace App\UserModule\Service;


class RegistrationFacade
{
	private \App\UserModule\Model\Repository\UserRepository $userRepository;
	private \Nette\Security\Passwords $passwords;

	public function __construct(
		\App\UserModule\Model\Repository\UserRepository $userRepository,
		\Nette\Security\Passwords $passwords
	)
	{
		$this->userRepository = $userRepository;
		$this->passwords = $passwords;
	}

	public function register(string $email, string $password): \App\UserModule\Model\Entity\User
	{
		return $this->userRepository->createNewUser($email, $this->passwords->hash($password));
	}

}
