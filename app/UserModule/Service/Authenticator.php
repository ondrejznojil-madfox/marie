<?php declare(strict_types = 1);

namespace App\UserModule\Service;


final class Authenticator implements \Nette\Security\IAuthenticator
{

	private \Nette\Security\Passwords $passwords;
	private \App\UserModule\Model\Repository\UserRepository $userRepository;


	public function __construct(
		\App\UserModule\Model\Repository\UserRepository $userRepository,
		\Nette\Security\Passwords $passwords
	)
	{
		$this->passwords = $passwords;
		$this->userRepository = $userRepository;
	}


	public function authenticate(array $credentials): \Nette\Security\IIdentity
	{
		if ( ! isset($credentials[0]) || ! isset($credentials[1])) {
			Throw new \InvalidArgumentException("Invalid username and/or password.");
		}

		$email = $credentials[0];
		$password = $credentials[1];

		$user = $this->userRepository->fetchByEmail($email);

		if ( ! $user) {
			throw new \Nette\Security\AuthenticationException('Username does not exists.');
		} elseif ( ! $this->passwords->verify($password, $user->getPassword())) {
			throw new \Nette\Security\AuthenticationException('Entered password is incorrect.');
		}

		$roles = [
			$user->getRole()->getUid(),
		];

		return new \Nette\Security\Identity($user->getId(), $roles, []);
	}
}
