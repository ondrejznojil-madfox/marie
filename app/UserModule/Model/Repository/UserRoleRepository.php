<?php declare(strict_types = 1);

namespace App\UserModule\Model\Repository;


class UserRoleRepository extends \Mdfx\Doctrine\Model\Repository\BaseRepository
{

	protected static ?string $entityClassName = \App\UserModule\Model\Entity\UserRole::class;


}
