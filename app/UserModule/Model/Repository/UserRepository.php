<?php declare(strict_types = 1);

namespace App\UserModule\Model\Repository;


class UserRepository extends \Mdfx\Doctrine\Model\Repository\BaseRepository
{

	protected static ?string $entityClassName = \App\UserModule\Model\Entity\User::class;

	private \Nette\Security\Passwords $passwords;

	public function __construct(
		\Mdfx\Doctrine\Model\EntityManager $em,
		\Nette\Security\Passwords $passwords
	)
	{
		parent::__construct($em);
		$this->passwords = $passwords;
	}

	public function fetch(int $id): ?\App\UserModule\Model\Entity\User
	{
		/** @var \App\UserModule\Model\Entity\User|NULL $entity */
		$entity = parent::fetch($id);

		return $entity;
	}

	private function getBaseQuery(): \Doctrine\ORM\QueryBuilder
	{
		$b = $this->em->createQueryBuilder();

		$b->select('u')
			->from(\App\UserModule\Model\Entity\User::class, 'u')
			->andWhere('u.isDeleted = :isDeleted')
			->setParameter('isDeleted', FALSE)
		;

		return $b;
	}


	public function fetchByEmail(string $email): ?\App\UserModule\Model\Entity\User
	{
		$b = $this->getGridQuery();

		$b
			->andWhere('u.email = :email')
			->setParameter('email', $email)
			->setMaxResults(1)
		;

		return $b->getQuery()->getOneOrNullResult();
	}


	public function getGridQuery(): \Doctrine\ORM\QueryBuilder
	{
		return $this->getBaseQuery();
	}


	private function getDefaultUserRole(): \App\UserModule\Model\Entity\UserRole
	{
		$b = $this->em->createQueryBuilder()
			->select('ur')
			->from(\App\UserModule\Model\Entity\UserRole::class, 'ur')
			->where('ur.uid = :uid')
			->setParameter('uid', \App\UserModule\Model\Entity\UserRole::UID_USER)
		;

		return $b->getQuery()->getSingleResult();
	}


	public function createNewUser(string $email, string $password): \App\UserModule\Model\Entity\User
	{
		$defaultRole = $this->getDefaultUserRole();

		$user = new \App\UserModule\Model\Entity\User(
			$email,
			$password,
			$defaultRole
		);

		$this->em->persist($user);
		$this->em->flush();

		return $user;
	}

	public function changePassword(\App\UserModule\Model\Entity\User $user, string $password): void
	{
		$user->setPassword($this->passwords->hash($password));

		$this->em->persist($user);
		$this->em->flush();
	}


	/**
	 * @return \App\UserModule\Model\Entity\User[]
	 */
	public function getNewsletterUsers(): array
	{
		$b = $this->getGridQuery();

		$b
			->andWhere('u.isMailing = TRUE');

		return $b->getQuery()->getResult();
	}

	public function unsubscribeUser(string $email): void
	{
		$user = $this->fetchByEmail($email);

		if ( ! $user instanceof \App\UserModule\Model\Entity\User) {
			return;
		}

		$user->setIsMailing(FALSE);

		$this->persist($user);
		$this->flush();
	}

	public function subscribeUser(\App\UserModule\Model\Entity\User $user): void
	{
		$user->setIsMailing(TRUE);

		$this->persist($user);
		$this->flush();
	}

}
