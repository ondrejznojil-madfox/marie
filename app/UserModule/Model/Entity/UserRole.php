<?php declare(strict_types = 1);

namespace App\UserModule\Model\Entity;


class UserRole extends \Mdfx\Doctrine\Model\Entity\BaseEntity
{

	public const USER_ID = 1;

	public const UID_USER = 'USER';
	public const UID_ADMIN = 'ADMIN';
	public const UID_DEV = 'DEV';

	protected string $uid;

	public function __construct(
		string $uid
	)
	{
		parent::__construct();

		$this->uid = $uid;
	}

	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('uid', 'string')
			->unique()
			->build();
	}

	public function getUid(): string
	{
		return $this->uid;
	}

	public function setUid(string $uid): void
	{
		$this->uid = $uid;
	}

}
