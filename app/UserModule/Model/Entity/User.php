<?php declare(strict_types = 1);

namespace App\UserModule\Model\Entity;

class User extends \Mdfx\Doctrine\Model\Entity\BaseEntity
{

	private string $email;

	private string $password;

	private UserRole $role;


	public function __construct(
		string $email,
		string $password,
		UserRole $userRole
	)
	{
		parent::__construct();

		$this->email = $email;
		$this->password = $password;
		$this->role = $userRole;
	}


	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('email', 'string')
			->unique()
			->build()
		;

		$builder->addField('password', 'string');

		$builder->createManyToOne('role', UserRole::class)
			->addJoinColumn('user_role_id', 'id', FALSE)
			->fetchLazy()
			->cascadeAll()
			->build()
		;
	}


	public function getEmail(): string
	{
		return $this->email;
	}

	public function setEmail(string $email): void
	{
		$this->email = $email;
	}

	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword(string $password): void
	{
		$this->password = $password;
	}

	public function getRole(): \App\UserModule\Model\Entity\UserRole
	{
		return $this->role;
	}

	public function setRole(\App\UserModule\Model\Entity\UserRole $role): void
	{
		$this->role = $role;
	}

}
