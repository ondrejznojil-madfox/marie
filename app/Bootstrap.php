<?php

declare(strict_types=1);

namespace App;

use Nette\Configurator;


class Bootstrap
{
	public static function boot(): Configurator
	{
		$configurator = new Configurator;

		$configurator->enableTracy(__DIR__ . '/../log');

		$configurator->setTimeZone('Europe/Prague');
		$configurator->setTempDirectory(__DIR__ . '/../temp');

		$configurator->createRobotLoader()
			->addDirectory(__DIR__)
			->register();

		$configurator
			->addConfig(__DIR__ . '/config/common.neon')
			->addConfig(__DIR__ . '/config/local.neon');

		foreach (self::getDimensionFiles() as $dimensionFile) {
			$configurator->addConfig($dimensionFile);
		}

		$decoded = \Nette\Neon\Neon::decode((string) \file_get_contents(__DIR__ . '/config/local.neon'));

		$debugMode = $decoded['parameters']['debug'] ?? FALSE;
		$configurator->setDebugMode($debugMode);
		$configurator->enableTracy(__DIR__ . '/../log');

		return $configurator;
	}

	private static function getDimensionFiles(): array
	{
		/** @var \SplFileInfo[] $dimensionsFiles */
		$dimensionsFiles = \Nette\Utils\Finder::findFiles('*.neon')
			->from(__DIR__ . '/ImageModule/dimensions')
		;

		$paths = [];
		foreach ($dimensionsFiles as $dimensionsFile) {
			$paths[] = $dimensionsFile->getPathname();
		}

		return $paths;
	}
}
