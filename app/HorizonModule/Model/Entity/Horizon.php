<?php declare(strict_types = 1);

namespace App\HorizonModule\Model\Entity;

class Horizon extends \Mdfx\Doctrine\Model\Entity\BaseEntity
{

	private \App\ImageModule\Model\Entity\Image $image;

	private int $position;

	private string $color;

	private TestImage $testImage;


	public function __construct(
		TestImage $testImage,
		\App\ImageModule\Model\Entity\Image $image,
		int $position,
		string $color,
	)
	{
		parent::__construct();

		$this->image = $image;
		$this->position = $position;
		$this->color = $color;
		$this->testImage = $testImage;
	}


	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('position', 'integer')
			->build()
		;

		$builder->createField('color', 'string')
			->build()
		;

		$builder->createManyToOne('image', \App\ImageModule\Model\Entity\Image::class)
			->addJoinColumn('image_id', 'id', FALSE)
			->fetchLazy()
			->cascadeAll()
			->build()
		;

		$builder->createManyToOne('testImage', TestImage::class)
			->addJoinColumn('test_image_id', 'id', FALSE, FALSE, 'CASCADE')
			->inversedBy('horizons')
			->fetchLazy()
			->build()
		;
	}


	public function getImage(): \App\ImageModule\Model\Entity\Image
	{
		return $this->image;
	}


	public function setImage(\App\ImageModule\Model\Entity\Image $image): void
	{
		$this->image = $image;
	}


	public function getPosition(): int
	{
		return $this->position;
	}


	public function setPosition(int $position): void
	{
		$this->position = $position;
	}


	public function getColor(): string
	{
		return $this->color;
	}


	public function setColor(string $color): void
	{
		$this->color = $color;
	}


	public function getTestImage(): TestImage
	{
		return $this->testImage;
	}


	public function setTestImage(TestImage $testImage): void
	{
		$this->testImage = $testImage;
	}

}
