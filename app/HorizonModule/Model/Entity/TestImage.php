<?php declare(strict_types = 1);

namespace App\HorizonModule\Model\Entity;

class TestImage extends \Mdfx\Doctrine\Model\Entity\BaseEntity
{

	private \Doctrine\Common\Collections\Collection $horizons;

	private Session $session;

	private int $position;

	private TestCase $testCase;


	public function __construct(
		Session $session,
		TestCase $testCase,
		int $position,
	)
	{
		parent::__construct();

		$this->horizons = new \Doctrine\Common\Collections\ArrayCollection();
		$this->session = $session;
		$this->position = $position;
		$this->testCase = $testCase;
	}


	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('position', 'integer')
			->build()
		;

		$builder->createOneToMany('horizons', Horizon::class)
			->addJoinColumn('test_image_id', 'id', FALSE, FALSE, 'CASCADE')
			->mappedBy('testImage')
			->fetchLazy()
			->build()
		;

		$builder->createManyToOne('session', Session::class)
			->addJoinColumn('session_id', 'id', FALSE, FALSE, 'CASCADE')
			->inversedBy('testImages')
			->fetchLazy()
			->build()
		;

		$builder->createManyToOne('testCase', TestCase::class)
			->addJoinColumn('test_case_id', 'id', FALSE, FALSE, 'CASCADE')
			->inversedBy('testImages')
			->fetchLazy()
			->build()
		;
	}


	/**
	 * @return \Doctrine\Common\Collections\Collection<\App\HorizonModule\Model\Entity\Horizon>
	 */
	public function getHorizons(): \Doctrine\Common\Collections\Collection
	{
		return $this->horizons
			->matching(\Mdfx\Doctrine\Model\CriteriaBuilder::createNonDeletedCriteria())
		;
	}

	public function getPosition(): int
	{
		return $this->position;
	}

	public function setPosition(int $position): void
	{
		$this->position = $position;
	}

	public function getTestCase(): TestCase
	{
		return $this->testCase;
	}

	public function setTestCase(TestCase $testCase): void
	{
		$this->testCase = $testCase;
	}

}
