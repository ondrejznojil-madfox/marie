<?php declare(strict_types = 1);

namespace App\HorizonModule\Model\Entity;

class TestCase extends \Mdfx\Doctrine\Model\Entity\BaseEntity
{

	private int $position;

	private \App\ImageModule\Model\Entity\Image $image;

	private ?string $name;

	private \Doctrine\Common\Collections\Collection $testImages;


	public function __construct(
		int $position,
		\App\ImageModule\Model\Entity\Image $image
	)
	{
		parent::__construct();

		$this->image = $image;
		$this->name = NULL;
		$this->testImages = new \Doctrine\Common\Collections\ArrayCollection();
	}


	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('position', 'integer')
			->unique()
			->build()
		;

		$builder->createField('name', 'string')
			->nullable()
			->build()
		;

		$builder->createManyToOne('image', \App\ImageModule\Model\Entity\Image::class)
			->addJoinColumn('image_id', 'id', FALSE)
			->fetchLazy()
			->cascadeAll()
			->build()
		;

		$builder->createOneToMany('testImages', TestImage::class)
			->addJoinColumn('test_image_id', 'id', FALSE, FALSE, 'CASCADE')
			->mappedBy('testCase')
			->fetchLazy()
			->build()
		;
	}

	/**
	 * @return \App\ImageModule\Model\Entity\Image
	 */
	public function getImage(): \App\ImageModule\Model\Entity\Image
	{
		return $this->image;
	}

	public function setImage(\App\ImageModule\Model\Entity\Image $image): void
	{
		$this->image = $image;
	}

	public function getPosition(): int
	{
		return $this->position;
	}

	public function setPosition(int $position): void
	{
		$this->position = $position;
	}

	public function getTestImages(): \Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection
	{
		return $this->testImages;
	}

	public function setTestImages(\Doctrine\Common\Collections\ArrayCollection|\Doctrine\Common\Collections\Collection $testImages): void
	{
		$this->testImages = $testImages;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(?string $name): void
	{
		$this->name = $name;
	}

}
