<?php declare(strict_types = 1);

namespace App\HorizonModule\Model\Entity;

class Session extends \Mdfx\Doctrine\Model\Entity\BaseEntity
{

	private \Doctrine\Common\Collections\Collection $testImages;

	private int $currentPosition;

	private bool $isFinished;


	public function __construct()
	{
		parent::__construct();

		$this->testImages = new \Doctrine\Common\Collections\ArrayCollection();
		$this->isFinished = FALSE;
		$this->currentPosition = 1;
	}


	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('isFinished', 'boolean')
			->option('default', 0)
			->build()
		;

		$builder->createField('currentPosition', 'integer')
			->option('default', 1)
			->build()
		;

		$builder->createOneToMany('testImages', TestImage::class)
			->addJoinColumn('test_image_id', 'id', FALSE, FALSE, 'CASCADE')
			->mappedBy('session')
			->fetchLazy()
			->build()
		;
	}


	/**
	 * @return \Doctrine\Common\Collections\Collection<\App\HorizonModule\Model\Entity\TestImage>
	 */
	public function getTestImages(): \Doctrine\Common\Collections\Collection
	{
		return $this->testImages;
	}

	public function isFinished(): bool
	{
		return $this->isFinished;
	}

	public function setIsFinished(bool $isFinished): void
	{
		$this->isFinished = $isFinished;
	}

	public function getCurrentPosition(): int
	{
		return $this->currentPosition;
	}

	public function setCurrentPosition(int $currentPosition): void
	{
		$this->currentPosition = $currentPosition;
	}

}
