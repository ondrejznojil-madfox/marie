<?php declare(strict_types = 1);

namespace App\HorizonModule\Model\Repository;

final class TestCaseRepository extends \Mdfx\Doctrine\Model\Repository\BaseRepository
{

	protected static ?string $entityClassName = \App\HorizonModule\Model\Entity\TestCase::class;

	public function getGridQuery(): \Doctrine\ORM\QueryBuilder
	{
		$b = $this->em->createQueryBuilder();

		$b
			->select('t')
			->from($this->getEntityName(), 't')
			->where('t.isDeleted = FALSE')
			->orderBy('t.position', 'ASC')
		;

		return $b;
	}

	public function fetchByPosition(int $position): ?\App\HorizonModule\Model\Entity\TestCase
	{
		/** @var \App\HorizonModule\Model\Entity\TestCase|NULL $testCase */
		$testCase = $this->repository->findOneBy([
			'position' => $position,
		]);

		return $testCase;
	}

	public function getCasesCount(): int
	{
		return $this->repository->count([
			'isDeleted' => FALSE,
		]);
	}

}
