<?php declare(strict_types = 1);

namespace App\HorizonModule\Model\Repository;

final class TestImageRepository extends \Mdfx\Doctrine\Model\Repository\BaseRepository
{

	protected static ?string $entityClassName = \App\HorizonModule\Model\Entity\TestImage::class;


	public function fetchByPosition(int $sessionId, int $position): ?\App\HorizonModule\Model\Entity\TestImage
	{
		/** @var \App\HorizonModule\Model\Entity\TestImage|NULL $testCase */
		$testCase = $this->repository->findOneBy([
			'position' => $position,
			'session' => $sessionId,
		]);

		return $testCase;
	}


	public function addHorizon(
		\App\HorizonModule\Model\Entity\TestImage $testImage,
		\App\ImageModule\Model\Entity\Image $image,
	): void
	{
		$newPosition = $this->getMaxHorizonPosition($testImage) + 1;

		$horizon = new \App\HorizonModule\Model\Entity\Horizon(
			$testImage,
			$image,
			$newPosition,
			\App\HorizonModule\Helper\ColorGetter::get($newPosition)
		);

		$this->persist($horizon);
		$this->flush();
	}


	public function getMaxHorizonPosition(\App\HorizonModule\Model\Entity\TestImage $testImage): int
	{
		$actualHorizons = $testImage->getHorizons();

		$position = 0;
		foreach ($actualHorizons as $horizon) {
			if ($horizon->getPosition() > $position) {
				$position = $horizon->getPosition();
			}
		}

		return $position;
	}

}
