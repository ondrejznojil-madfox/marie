<?php declare(strict_types = 1);

namespace App\HorizonModule\Model\Repository;

final class SessionRepository extends \Mdfx\Doctrine\Model\Repository\BaseRepository
{

	protected static ?string $entityClassName = \App\HorizonModule\Model\Entity\Session::class;

}
