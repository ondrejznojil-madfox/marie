<?php declare(strict_types = 1);

namespace App\HorizonModule\Session;

final class SessionCreator
{

	public function __construct(
		private \App\HorizonModule\Model\Repository\SessionRepository $sessionRepository,
	)
	{
	}


	public function create(): \App\HorizonModule\Model\Entity\Session
	{
		$session = new \App\HorizonModule\Model\Entity\Session();

		$this->sessionRepository->persist($session);
		$this->sessionRepository->flush();

		return $session;
	}

}
