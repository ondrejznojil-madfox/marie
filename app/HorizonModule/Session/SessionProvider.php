<?php declare(strict_types = 1);

namespace App\HorizonModule\Session;

final class SessionProvider
{

	private const SESSION_NAME = 'session';
	private const SESSION_ID = 'sessionId';

	private \Nette\Http\SessionSection $session;

	public function __construct(
		\Nette\Http\Session $session,
		private \App\HorizonModule\Model\Repository\SessionRepository $sessionRepository,
		private SessionCreator $sessionCreator,
	)
	{
		$this->session = $session->getSection(self::SESSION_NAME);
	}

	private function getSessionId(): ?int
	{
		$id = $this->session->offsetGet(self::SESSION_ID);

		return \is_numeric($id) ? (int) $id : NULL;
	}

	private function setSessionId(?int $id): void
	{
		$this->session->offsetSet(self::SESSION_ID, $id);
	}

	public function provide(): \App\HorizonModule\Model\Entity\Session
	{
		$sessionId = $this->getSessionId();

		if ($sessionId) {
			$session = $this->sessionRepository->fetch($sessionId);

			if ($session instanceof \App\HorizonModule\Model\Entity\Session && ! $session->isFinished()) {
				return $session;
			}
		}

		$session = $this->sessionCreator->create();

		$this->setSessionId((int) $session->getId());

		return $session;
	}

}
