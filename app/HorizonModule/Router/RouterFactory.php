<?php declare(strict_types = 1);

namespace App\HorizonModule\Router;

final class RouterFactory
{

	public function createRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('Horizon');

		$router->add($this->createFrontRouter());
		$router->add($this->createAdminRouter());

		return $router;
	}

	private function createFrontRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('Front');

		$router->addRoute('/test', 'Horizon:default');
		$router->addRoute('/test/back', 'Horizon:back');
		$router->addRoute('/test/continue', 'Horizon:continue');
		$router->addRoute('/test/resume/<id>', 'Resume:default');

		return $router;
	}

	private function createAdminRouter(): \Nette\Application\Routers\RouteList
	{
		$router = new \Nette\Application\Routers\RouteList('Admin');

		$router->addRoute('/admin/test-cases', 'CaseGrid:default');
		$router->addRoute('/admin/test-cases/edit[/<id>]', 'CaseEdit:default');
		$router->addRoute('/admin/test-cases/delete[/<id>]', 'CaseGrid:delete');
		$router->addRoute('/admin/test-cases/undo-deletion[/<id>]', 'CaseGrid:undoRemove');

		return $router;
	}

}
