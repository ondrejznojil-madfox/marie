<?php declare(strict_types = 1);

namespace App\HorizonModule\Helper;

final class ColorGetter
{
	private const MAP = [
		1 => 'FF5733',
		2 => '336EFF',
		3 => '33FF3C',
		4 => 'F933FF',
		5 => 'F9FF33',
		6 => '9C33FF',
	];

	private const DEFAULT_COLOR = '#000000';


	public static function get(int $position): string
	{
		return self::MAP[$position] ?? self::DEFAULT_COLOR;
	}

}
