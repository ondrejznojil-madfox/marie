<?php declare(strict_types=1);

namespace App\HorizonModule\FrontModule\Presenter;

final class HorizonPresenter extends \App\CoreModule\Presenter\BaseFrontPresenter
{

	private ?\App\HorizonModule\Model\Entity\Session $session = NULL;

	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseFrontPresenterFacade $baseFrontPresenterFacade,
		private \App\HorizonModule\Session\SessionProvider $sessionProvider,
		private \App\HorizonModule\Model\Repository\TestCaseRepository $testCaseRepository,
		private \App\HorizonModule\Model\Repository\TestImageRepository $testImageRepository,
		private \App\HorizonModule\Model\Repository\HorizonRepository $horizonRepository,
		private \App\HorizonModule\FileHandler\FileHandler $fileHandler,
	)
	{
		parent::__construct($baseFrontPresenterFacade);
	}

	public function startup(): void
	{
		parent::startup();

		$this->session = $this->sessionProvider->provide();
	}

	public function renderDefault(): void
	{
		$totalCount = $this->testCaseRepository->getCasesCount();

		if ($totalCount < $this->session->getCurrentPosition()) {
			$this->session->setIsFinished(TRUE);
			$this->horizonRepository->persist($this->session);
			$this->horizonRepository->flush();

			$this->redirect(':Horizon:Front:Resume:', [
				'id' => $this->session->getId(),
			]);
		}

		$testCase = $this->testCaseRepository->fetchByPosition($this->session->getCurrentPosition());

		if ( ! $testCase instanceof \App\HorizonModule\Model\Entity\TestCase) {
			throw new \InvalidArgumentException('něco se pokazilo');
		}

		$testImage = $this->testImageRepository->fetchByPosition($this->session->getId(), $this->session->getCurrentPosition());

		if ( ! $testImage instanceof \App\HorizonModule\Model\Entity\TestImage) {
			$testImage = new \App\HorizonModule\Model\Entity\TestImage(
				$this->session,
				$testCase,
				$this->session->getCurrentPosition(),
			);

			$this->testImageRepository->persist($testImage);
			$this->testImageRepository->flush();
		}

		$images = \array_map(
			fn (\App\HorizonModule\Model\Entity\Horizon $horizon): string => $horizon->getImage()->getUrl(),
			$testImage->getHorizons()->toArray(),
		);

		$newHorizonPosition = $this->testImageRepository->getMaxHorizonPosition($testImage);

		$maxWidth = 1232;
		$imageWidth = $testCase->getImage()->getWidth();
		$imageHeight = $testCase->getImage()->getHeight();

		if ($imageWidth > $maxWidth) {
			$ratio = $imageHeight / $imageWidth;
			$imageWidth = $maxWidth;
			$imageHeight = \intval($maxWidth * $ratio);
		}

		$this->getTemplate()->setParameters([
			'isLast' => $this->session->getCurrentPosition() === $totalCount,
			'uploadLink' => $this->link('upload!'),
			'color' => \App\HorizonModule\Helper\ColorGetter::get($newHorizonPosition + 1),
			'totalCount' => $totalCount,
			'currentPosition' => $this->session->getCurrentPosition(),
			'sessionId' => $this->session->getId(),
			'currentImageUrl' => $testCase->getImage()->getUrl(),
			'horizons' => $testImage->getHorizons(),
			'images' => \Nette\Utils\Json::encode($images),
			'imageWidth' => $imageWidth,
			'imageHeight' => $imageHeight,
		]);

		$this->redrawControl();
	}

	public function handleRemoveHorizon(int $horizonId): void
	{
		$horizon = $this->horizonRepository->fetch($horizonId);

		if ($horizon) {
			$this->horizonRepository->remove($horizon);
		}

		$this->redrawControl();
	}

	public function actionBack(): void
	{
		$this->session->setCurrentPosition($this->session->getCurrentPosition() - 1);
		$this->horizonRepository->persist($this->session);
		$this->horizonRepository->flush();

		$this->redirect('default');
	}


	public function actionContinue(): void
	{
		$this->session->setCurrentPosition($this->session->getCurrentPosition() + 1);
		$this->horizonRepository->persist($this->session);
		$this->horizonRepository->flush();

		$this->redirect('default');
	}


	public function handleUpload(): void
	{
		$this->fileHandler->handle($this->session);

		$this->redrawControl();
	}

}
