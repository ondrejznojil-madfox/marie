<?php declare(strict_types=1);

namespace App\HorizonModule\FileHandler;

class FileHandler
{

	public function __construct(
		private \Nette\Http\Request $request,
		private \App\MediaModule\Service\MediaUploader $mediaUploader,
		private \App\MediaModule\Model\Repository\ImageRepository $imageRepository,
		private \App\HorizonModule\Model\Repository\TestImageRepository $testImageRepository,
	)
	{
	}

	public function handle(\App\HorizonModule\Model\Entity\Session $session): void
	{
		$files = $this->request->getFiles();

		$fileUpload = \reset($files);

		if ( ! $fileUpload instanceof \Nette\Http\FileUpload) {
			throw new \Nette\Application\BadRequestException("File upload data not found");
		}

		if ( ! $fileUpload->isImage() || ! $fileUpload->getContents()) {
			throw new \Nette\Application\BadRequestException("Uploaded file is not an image.");
		}

		$imageResponse = $this->mediaUploader->upload((string) $fileUpload->getContents(), 'wisywyg.png');

		$image = $this->saveImage($imageResponse, $fileUpload);

		$testImage = $this->testImageRepository->fetchByPosition($session->getId(), $session->getCurrentPosition());

		$this->testImageRepository->addHorizon($testImage, $image);
	}

	private function saveImage(
		\App\MediaModule\DTO\UploadResponse $uploadResponse,
		\Nette\Http\FileUpload $fileUpload
	): \App\ImageModule\Model\Entity\Image
	{
		$uploadImage = $fileUpload->toImage();

		$dbImage = new \App\ImageModule\Model\Entity\Image(
			$uploadResponse->getKey(),
			$uploadResponse->getUrl(),
		);

		$dbImage->setWidth($uploadImage->getWidth());
		$dbImage->setHeight($uploadImage->getHeight());

		$this->imageRepository->persist($dbImage);
		$this->imageRepository->flush();

		return $dbImage;
	}

}
