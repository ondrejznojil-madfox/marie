<?php declare(strict_types=1);

namespace App\HorizoneModule\AdminModule\TestCase\EditForm;

final class FormData
{

	public ?string $name;

	public ?int $position;

	public \Nette\Http\FileUpload $image;


	public function getName(): ?string
	{
		return $this->name;
	}

	public function getPosition(): int
	{
		return $this->position;
	}

	public function getUpload(): \Nette\Http\FileUpload
	{
		return $this->image;
	}

}
