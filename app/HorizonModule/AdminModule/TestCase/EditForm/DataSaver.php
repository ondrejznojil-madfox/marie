<?php declare(strict_types=1);

namespace App\HorizoneModule\AdminModule\TestCase\EditForm;

final class DataSaver
{

	public function __construct(
		private \App\HorizonModule\Model\Repository\TestCaseRepository $testCaseRepository,
		private \App\MediaModule\Service\MediaUploader $mediaUploader,
	)
	{
	}

	public function save(
		FormData $formData,
		?\App\HorizonModule\Model\Entity\TestCase $testCase,
	): \App\HorizonModule\Model\Entity\TestCase
	{
		if ( ! $testCase) {
			$image = $this->processImage($formData->getUpload());

			$testCase = new \App\HorizonModule\Model\Entity\TestCase(
				$formData->getPosition(),
				$image,
			);
		} else {
			if ($formData?->getUpload()->isOk()) {
				$image = $this->processImage($formData->getUpload());

				$currentImage = $testCase?->getImage();

				if ($currentImage) {
					$this->testCaseRepository->remove($currentImage);
				}

				$testCase->setImage($image);
			}
		}

		$testCase->setPosition($formData->getPosition());
		$testCase->setName($formData->getName());

		$this->testCaseRepository->persist($testCase);
		$this->testCaseRepository->flush();

		return $testCase;
	}

	private function processImage(
		?\Nette\Http\FileUpload $upload,
	): \App\ImageModule\Model\Entity\Image
	{
		if ( ! $upload instanceof \Nette\Http\FileUpload || ! $upload->isOk()) {
			throw new \UnexpectedValueException('Nepodařilo se nahrát obrázek.');
		}

		$uploadData = $this->mediaUploader->upload((string) $upload->getContents(), $upload->getName());

		$image = new \App\ImageModule\Model\Entity\Image($uploadData->getKey(), $uploadData->getUrl());
		$fileUpload = $upload->toImage();
		$image->setWidth($fileUpload->getWidth());
		$image->setHeight($fileUpload->getHeight());

		$this->testCaseRepository->persist($image);

		return $image;
	}

}
