<?php declare(strict_types=1);

namespace App\HorizoneModule\AdminModule\TestCase\EditForm;

final class DataSetter
{

	public function set(\Nette\Forms\Form $form, \App\HorizonModule\Model\Entity\TestCase $testCase): void
	{
		$data = [
			'name' => $testCase->getName(),
			'position' => $testCase->getPosition(),
		];

		$form->setDefaults($data);
	}

}
