<?php declare(strict_types=1);

namespace App\HorizoneModule\AdminModule\TestCase\EditForm;

final class FormFactory
{

	public function __construct(
		private \App\CoreModule\Service\Form\GeneralFormFactory $generalFormFactory,
		private DataSetter $dataSetter
	)
	{
	}

	public function create(?\App\HorizonModule\Model\Entity\TestCase $testCase): \Nette\Application\UI\Form
	{
		$form = $this->generalFormFactory->create();
		$form->setMappedType(FormData::class);

		$form->addInteger('position', 'Pozice')
			->setRequired('Je nutné vyplnit unikátní číselnou pozici tohoto test-casu.')
		;

		$form->addText('name', 'Pracovní název')
			->setRequired(FALSE)
		;

		$form->addUpload('image', 'Obrázek');
		$form->addSubmit('submit');

		if ($testCase instanceof \App\HorizonModule\Model\Entity\TestCase) {
			$this->dataSetter->set($form, $testCase);
		}

		return $form;
	}

}
