<?php declare(strict_types = 1);

namespace App\HorizoneModule\AdminModule\TestCase\Grid;

final class GridFactory
{

	public function __construct(
		private \App\CoreModule\AdminModule\Grid\GridFactory $gridFactory,
		private \App\HorizonModule\Model\Repository\TestCaseRepository $testCaseRepository
	)
	{
	}

	public function create(): \Mdfx\Datagrid\DataGrid
	{
		$grid = $this->gridFactory->create();

		$grid->setDataSource($this->testCaseRepository->getGridQuery());
		$grid->addColumnText('id', '#');

		$grid->addColumnText('name', 'Název');

		$grid->addColumnNumber('respondentCount', 'Počet respondentů', 'createdAt')
			->setRenderer(function (\App\HorizonModule\Model\Entity\TestCase $case): string {
				return (string) $case->getTestImages()->count();
		});

		$grid->addColumnNumber('position', 'Pořadí');

		$grid->addEditAction('edit', 'Upravit', ':Horizon:Admin:CaseEdit:default');

		return $grid;
	}

}
