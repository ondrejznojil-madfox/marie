<?php declare(strict_types = 1);

namespace App\HorizonModule\AdminModule\Presenter;

final class CaseEditPresenter extends \App\CoreModule\Presenter\BaseAdminPresenter
{

	private ?\App\HorizonModule\Model\Entity\TestCase $testCase;


	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseAdminPresenterFacade $baseAdminPresenterFacade,
		private \App\HorizonModule\Model\Repository\TestCaseRepository $testCaseRepository,
		private \App\HorizoneModule\AdminModule\TestCase\EditForm\FormFactory $formFactory,
		private \App\HorizoneModule\AdminModule\TestCase\EditForm\DataSaver $dataSaver,
	)
	{
		parent::__construct($baseAdminPresenterFacade);
	}

	public function actionDefault(?int $id): void
	{
		$this->testCase = $id ? $this->testCaseRepository->fetch($id) : NULL;
	}


	public function renderDefault(?int $id): void
	{
		$this->getTemplate()->setParameters([
			'id' => $id,
			'name' => $this->testCase?->getName(),
			'lastUpdateDate' => $this->testCase?->getUpdatedAt(),
			'imageUrl' => $this->testCase?->getImage()?->getUrl(),
		]);
	}


	protected function createComponentForm(): \Nette\Application\UI\Form
	{
		$form = $this->formFactory->create($this->testCase);

		$form->onSuccess[] = function (
			\Nette\Forms\Form $form,
			\App\HorizoneModule\AdminModule\TestCase\EditForm\FormData $formData
		): void {
			try {
				$newArticle = $this->dataSaver->save($formData, $this->testCase);

				$this->flashMessage("Uložení proběhlo úspěšně", 'ok');
			} catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException) {
				$this->flashMessage('Tato pozice je už použita, zvolte prosím jinou.', 'error');

				return;
			} catch (\Exception $e) {
				$this->flashMessage($e->getMessage(), 'error');

				return;
			}

			$this->redirect(':Horizon:Admin:CaseEdit:', [
				'id' => $newArticle->getId(),
			]);

		};

		$form->onError[] = function (\Nette\Forms\Form $form): void {
			foreach ($form->getErrors() as $error) {
				$this->flashMessage($error, 'error');
			}
		};

		return $form;
	}

}
