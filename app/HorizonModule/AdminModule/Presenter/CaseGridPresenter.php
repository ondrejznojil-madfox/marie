<?php declare(strict_types = 1);

namespace App\HorizonModule\AdminModule\Presenter;

final class CaseGridPresenter extends \App\CoreModule\Presenter\BaseAdminPresenter
{

	public function __construct(
		\App\CoreModule\Presenter\Facade\BaseAdminPresenterFacade $baseAdminPresenterFacade,
		private \App\HorizoneModule\AdminModule\TestCase\Grid\GridFactory $gridFactory,
		private \App\HorizonModule\Model\Repository\TestCaseRepository $testCaseRepository,
		private \App\CoreModule\Service\UndoMessageGenerator $undoMessageGenerator,
	)
	{
		parent::__construct($baseAdminPresenterFacade);
	}


	protected function createComponentGrid(): \Mdfx\Datagrid\DataGrid
	{
		return $this->gridFactory->create();
	}


	public function actionDelete(int $id): void
	{
		$testCase = $this->testCaseRepository->fetch($id);

		if ( ! $testCase instanceof \App\HorizonModule\Model\Entity\TestCase) {
			$this->redirect('default');
		}

		$this->testCaseRepository->remove($testCase);

		$message = $this->undoMessageGenerator->generate($this->link('undoRemove', [
			'id' => $id,
		]));

		$this->flashMessage($message);
		$this->redirect('default');
	}


	public function actionUndoRemove(int $id): void
	{
		$testCase = $this->testCaseRepository->fetch($id);

		if ( ! $testCase instanceof \App\HorizonModule\Model\Entity\TestCase) {
			$this->redirect('default');
		}

		$this->testCaseRepository->undoRemove($testCase);

		$this->flashMessage('Položka byla obnovena.');
		$this->redirect('default');
	}

}
