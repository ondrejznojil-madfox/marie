<?php declare(strict_types = 1);

namespace App\MediaModule\Service;


class TimestampNamingStrategy implements FileNamingStrategy
{

	public function getName(string $filename): string
	{
		return \Nette\Utils\Random::generate(4) . \time() . '.' . \App\ImageModule\Helper\ExtensionGetter::getExtension($filename);
	}

}
