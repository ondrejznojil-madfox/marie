<?php declare(strict_types = 1);

namespace App\MediaModule\Service;


interface MediaUploader
{

	public function upload(string $data, string $filename): \App\MediaModule\DTO\UploadResponse;

	public function delete(\App\ImageModule\Model\Entity\Image $image): void;

}
