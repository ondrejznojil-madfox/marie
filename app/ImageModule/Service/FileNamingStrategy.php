<?php declare(strict_types = 1);

namespace App\MediaModule\Service;


interface FileNamingStrategy
{

	public function getName(string $filename): string;

}
