<?php declare(strict_types = 1);

namespace App\ImageModule\Service;

class MediaGetter
{

	private \Mdfx\PictureGenerator\Configuration\Configuration $configuration;


	public function __construct(
		\Mdfx\PictureGenerator\Configuration\Configuration $configuration
	)
	{
		$this->configuration = $configuration;
	}


	public function getRawUrl(string $key): string
	{
		return $this->configuration->getBaseUrl() . '/' . $key;
	}

}
