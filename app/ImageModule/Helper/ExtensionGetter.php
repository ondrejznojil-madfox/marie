<?php declare(strict_types = 1);

namespace App\ImageModule\Helper;


class ExtensionGetter
{

	public static function getExtension(string $filename): string
	{
		return \mb_substr(\strrchr($filename, '.') ?: '', 1);
	}

}
