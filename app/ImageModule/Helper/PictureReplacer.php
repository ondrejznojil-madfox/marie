<?php declare(strict_types=1);

namespace App\ImageModule\Helper;

class PictureReplacer
{

	private \Mdfx\PictureGenerator\Generator\PictureGeneratorFacade $pictureGeneratorFacade;
	private \App\MediaModule\Model\Repository\ImageRepository $imageRepository;

	public function __construct(
		\Mdfx\PictureGenerator\Generator\PictureGeneratorFacade $pictureGeneratorFacade,
		\App\MediaModule\Model\Repository\ImageRepository $imageRepository
	)
	{
		$this->pictureGeneratorFacade = $pictureGeneratorFacade;
		$this->imageRepository = $imageRepository;
	}

	public function replace(?string $content): string
	{
		$callback = fn (array $matches): string => $this->replaceImage($matches);

		return \preg_replace_callback('/<img src="(.*?)">/', $callback, $content);
	}


	private function replaceImage(array $matches): string
	{
		$src = $matches[1] ?? NULL;

		if ( ! $src) {
			return $mathces[0] ?? '';
		}

		$keyMatches = [];
		\preg_match('/https:\/\/.*\/(.*)\?/', $src, $keyMatches);

		$key = $keyMatches[1] ?? NULL;

		if ( ! \is_string($key)) {
			return $mathces[0] ?? '';
		}

		$image = new \Mdfx\PictureGenerator\StaticImage\StaticImage($key);

		$pictureHtml = \Mdfx\PictureGenerator\Macro\ImageMacro::imageHelper($image, ['size' => 'wysiwyg-default'], $this->pictureGeneratorFacade);

		return $this->addHeightToPicture($key, $pictureHtml);
	}


	private function addHeightToPicture(string $key, string $html): string
	{
		$image = $this->imageRepository->getImageByKey($key);

		if ( ! $image instanceof \App\ImageModule\Model\Entity\Image) {
			return $html;
		}

		$ratio = $image->getHeight() / $image->getWidth();

		return \preg_replace('/<div/', '<div style="padding-top: ' . $ratio * 100 . '%" ', $html, 1);
	}

}
