<?php declare(strict_types = 1);

namespace App\MediaModule\Model\Repository;


class ImageRepository extends \Mdfx\Doctrine\Model\Repository\BaseRepository
{

	protected static ?string $entityClassName = \App\ImageModule\Model\Entity\Image::class;

	public function getImageByKey(string $key): ?\App\ImageModule\Model\Entity\Image
	{
		$b = $this->em->createQueryBuilder();

		$b
			->select('i')
			->from(\App\ImageModule\Model\Entity\Image::class, 'i')
			->setMaxResults(1)
			->where('i.mediaKey = :key')
			->setParameter('key', $key)
		;

		return $b->getQuery()->getOneOrNullResult();
	}

}
