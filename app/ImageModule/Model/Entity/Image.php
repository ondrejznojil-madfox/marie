<?php declare(strict_types = 1);

namespace App\ImageModule\Model\Entity;


class Image extends \Mdfx\Doctrine\Model\Entity\BaseEntity
	implements \Mdfx\PictureGenerator\Contract\ImageInterface
{

	protected string $url;

	protected string $mediaKey;

	protected ?int $height;

	protected ?int $width;


	public function __construct(
		string $mediaKey,
		string $url
	)
	{
		parent::__construct();

		$this->height = NULL;
		$this->width = NULL;
		$this->mediaKey = $mediaKey;
		$this->url = $url;
	}


	public static function loadMetadata(\Doctrine\ORM\Mapping\ClassMetadata $metadata): void
	{
		$builder = \Mdfx\Doctrine\Model\Schema\BaseEntitySchema::build($metadata);

		$builder->createField('url', 'string')
			->build()
		;

		$builder->createField('width', 'integer')
			->nullable()
			->build()
		;

		$builder->createField('height', 'integer')
			->nullable()
			->build()
		;

		$builder->createField('mediaKey', 'string')
			->build()
		;
	}

	public function getMediaKey(): string
	{
		return $this->mediaKey;
	}

	public function setMediaKey(string $mediaKey): void
	{
		$this->mediaKey = $mediaKey;
	}

	public function getUrl(): string
	{
		return $this->url;
	}

	public function setUrl(string $url): void
	{
		$this->url = $url;
	}

	public function getKey(): string
	{
		return $this->getMediaKey();
	}


	public function getHeight(): ?int
	{
		return $this->height;
	}


	public function getWidth(): ?int
	{
		return $this->width;
	}


	public function setWidth(?int $width): void
	{
		$this->width = $width;
	}


	public function setHeight(?int $height): void
	{
		$this->height = $height;
	}

}
