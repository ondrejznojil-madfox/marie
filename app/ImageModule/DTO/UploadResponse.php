<?php declare(strict_types = 1);

namespace App\MediaModule\DTO;

class UploadResponse
{

	/**
	 * @var string
	 */
	private $url;
	/**
	 * @var string
	 */
	private $key;


	public function __construct(
		string $url,
		string $key
	)
	{
		$this->url = $url;
		$this->key = $key;
	}

	public function getKey(): string
	{
		return $this->key;
	}

	public function getUrl(): string
	{
		return $this->url;
	}

}
