
const isInViewport = (el, options = {}) => {
	if (!el) throw Error('isInViewport element not specified');

	const defaultOptions = {
		offsetTop: 0,
		offsetBottom: 0,
		origin: 'center'
	};

	const viewportHeight = window.innerHeight;
	const elHeight= el.offsetHeight;

	options = Object.assign({}, defaultOptions, options);

	let elRects = el.getBoundingClientRect();

	if (options.origin === 'center') {
		elRects.y = elRects.y - viewportHeight / 2 + elHeight / 2;
	}

	if (elRects.y < viewportHeight + options.offsetBottom && elRects.y > -viewportHeight - options.offsetTop) {
		return elRects;
	}

	return false;
};

export {
	isInViewport
};
