
export default class ChunkController {

    constructor() {
        const elements = document.querySelectorAll('[data-load-chunk]');

        elements.forEach(async (element) => {
            const chunkName = element.dataset.loadChunk;
            if (!chunkName) return;

            const chunkController = await import(/* webpackChunkName: "[request]" */ `../chunks/${chunkName}.js`);

            chunkController.init();
        });
    }
}
