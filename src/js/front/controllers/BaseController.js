import naja from 'naja';
import Animate from '../UI/Animate';
import {hasState} from '../../common/helpers/State';
import NetteValidator from '../UI/FormValidator';
import Sticky from '../UI/Sticky';
import HiddenBox from '../UI/HiddenBox';
import Modal from '../UI/Modal';

export default class BaseController {

    constructor() {
        this.initNaja();
        this.onStylesLoaded();
        this.initForms();

        Sticky.initGlobal();
        HiddenBox.initGlobal();
        Modal.initGlobal();
    }

    initNaja = () => {
        naja.initialize({
            history: false,
        });
    };

    onStylesLoaded = () => {
        if (!hasState(document.body, 'styles-loaded')) {
            if ("MutationObserver" in window) {
                const bodyMutationObserverCallback = (mutationsList) => {
                    mutationsList.forEach(mutation => {
                        if (mutation.attributeName === 'data-state') {
                            Animate.initGlobal();
                        }
                    });
                };

                const bodyMutationObserver = new MutationObserver(bodyMutationObserverCallback);

                bodyMutationObserver.observe(
                    document.body,
                    {attributes: true}
                );
            } else {
                setTimeout(() => {
                    Animate.initGlobal();
                }, 500);
            }
        } else {
            Animate.initGlobal();
        }
    };

    initForms() {
        Array.from(document.getElementsByTagName('form')).forEach(NetteValidator.init);
    }
}
