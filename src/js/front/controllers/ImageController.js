import Painterro from 'painterro'
import naja from 'naja'

export default class ImageController {
    painteroId = 'paintero'
    paintero

    init() {
        const options = {
            id: this.painteroId,
            defaultTool: 'brush',
            hiddenTools: ['crop', 'line', 'arrow', 'rect', 'ellipse', 'text', 'rotate', 'resize', 'open', 'close', 'zoomin', 'zoomout', 'pixelize', 'select', 'pixelize', 'settings', 'color-control'],
            defaultLineWidth: 10,
            defaultEraserWidth: 20,
            defaultArrowLength: 10,
        }

        const painteroElement = document.getElementById('paintero')

        if ( ! painteroElement) {
            return
        }

        options.backplateImgUrl = painteroElement.dataset.currentImage
        options.activeColor = painteroElement.dataset.color
        options.saveHandler = this.getSaveHandlerCallback(painteroElement.dataset.uploadLink)
        options.defaultSize = painteroElement.dataset.size

        this.paintero = Painterro(options)

        this.hideUnwantedControls()
        this.initHorizonImages()
        this.initExternalSaveButton()

        this.paintero.show()
    }

    initExternalSaveButton() {
        const saveButton = document.querySelector('#save-button')

        saveButton.addEventListener('click', (e) => {
            const originalSaveButton = document.getElementsByClassName('ptro-icon-save').item(0)

            originalSaveButton.click()
        })
    }

    initHorizonImages = () => {
        const paintero = document.getElementById('paintero')

        const targetEl = document.getElementsByClassName('ptro-crp-el').item(0)

        JSON.parse(paintero.dataset.horizons).forEach(this.createHorizonImage(targetEl))
    }

    createHorizonImage = (targetEl) => (url) => {
        const image = document.createElement('img')
        image.src = url
        image.classList.add('horizon-image')

        targetEl.append(image)
    }

    hideUnwantedControls = () => {
        const brushControls = document.getElementsByClassName('ptro-tool-controls')
        Array.from(brushControls).forEach(el => el.style.display = 'none')

        const infoControls = document.getElementsByClassName('ptro-info')
        Array.from(infoControls).forEach(el => el.style.display = 'none')

        const saveControls = document.getElementsByClassName('ptro-bar-right')
        Array.from(saveControls).forEach(el => el.style.display = 'none')
    }

    getSaveHandlerCallback = (uploadLink) => (image, done) => {
        const formData = new FormData();
        formData.append('image', image.asBlob());

        naja.makeRequest('POST', uploadLink, formData).then(() => done())
    }

    destroy = () => {
        this.paintero.hide()
    }
}
