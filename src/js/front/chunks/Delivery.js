import DeliveryMap from '../UI/DeliveryMap';

const init = () => {
    new DeliveryMap();
};

export {
    init
};
