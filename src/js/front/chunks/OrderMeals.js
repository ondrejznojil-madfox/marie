import {getElement} from '../../common/helpers/Element';
import Counter from '../UI/Counter';
import TotalPrice from '../UI/TotalPrice';
import SoupCount from '../UI/SoupCount';
import CounterPairs from "../UI/CounterPairs";

const init = () => {
    const form = getElement('order-meals-form');
    const buttonToTrigger = getElement('order-meals-update-price-btn');

    Counter.initGlobal();
    SoupCount.initGlobal();
    CounterPairs.initGlobal();

    if (form && buttonToTrigger) new TotalPrice(form, buttonToTrigger);
};

export {
    init
};
