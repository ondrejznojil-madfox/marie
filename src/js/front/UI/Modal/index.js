import {removeState, setState} from '../../../common/helpers/State';

const SELECTORS = {
    clone: '[data-modal-clone]',
    modal: '[data-modal]',
    open: (name) => `[data-modal-open~="${name}"]`,
    close: (name) => `[data-modal-close~="${name}"]`,
};

const STATES = {
    VISIBLE: 'visible'
};

class Modal {
    constructor(modalEl) {
        if (!modalEl) return;
        this.modalEl = modalEl;
        this.clone = document.querySelector(SELECTORS.clone);
        this.modalName = this.modalEl.dataset.modal;
        if (!this.modalName) return;

        this.openBtns = document.querySelectorAll(SELECTORS.open(this.modalName));
        this.closeBtns = [...document.querySelectorAll(SELECTORS.close(this.modalName)), this.clone];

        this.bind();
    }

    bind = () => {
        this.openBtns.forEach((button) => {
            button.addEventListener('click', this.open.bind(this));
        });
        this.closeBtns.forEach((button) => {
            button.addEventListener('click', this.close.bind(this));
        });
    };

    open = () => {
        setState(this.modalEl, STATES.VISIBLE);
        setState(this.clone, STATES.VISIBLE);
    };

    close = () => {
        removeState(this.modalEl, STATES.VISIBLE);
        removeState(this.clone, STATES.VISIBLE);
    };

    static initGlobal = () => {
        const modals = document.querySelectorAll(SELECTORS.modal);
        modals.forEach((modal) => new Modal(modal));
    }
}

export default Modal;
