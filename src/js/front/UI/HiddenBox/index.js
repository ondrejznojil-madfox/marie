import {toggleState} from '../../../common/helpers/State';

const SELECTORS = {
    trigger: (name) => `[data-hidden-box-trigger=${name}]`,
    target: '[data-hidden-box-target]'
};

const STATES = {
    hidden: 'hidden'
};

class HiddenBox {
    constructor(el) {
        if (!el) return;
        this.el = el;
        this.name = this.el.dataset.hiddenBoxTarget;

        this.triggers = document.querySelectorAll(SELECTORS.trigger(this.name));

        this.triggers.forEach((trigger) => {
            trigger.addEventListener('change', this.toggle.bind(this));
        });
    }

    toggle = () => {
        toggleState(this.el, STATES.hidden, this.el.checked);
    };

    static initGlobal = () => {
        const boxes = document.querySelectorAll(SELECTORS.target);
        boxes.forEach((box) => new HiddenBox(box));
    };
}

export default HiddenBox;
