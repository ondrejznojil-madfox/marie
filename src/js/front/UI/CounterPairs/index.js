import {toggleState} from "../../../common/helpers/State";

const SELECTORS = {
    counterInput: {
        el: 'data-counter-pairs',
        dataset: 'counterPairs'
    },
    targetWrapper: (id) => `data-counter-pairs-wrapper~=${id}`,
    targetInput: (id) => `data-counter-pairs-target~=${id}`,
};

const STATES = {
    show: 'show'
};

class CounterPairs {

    constructor(counterInput) {
        if (!counterInput) return;

        this.counterInput = counterInput;
        this.counterInputId = counterInput.dataset[SELECTORS.counterInput.dataset];

        this.wrapper = document.querySelector(`[${SELECTORS.targetWrapper(this.counterInputId)}]`);

        this.pairInput = document.querySelector(`[${SELECTORS.targetInput(this.counterInputId)}]`);

        this.counterInput.addEventListener('counterValueChange', this.onChange);
        this.counterInput.addEventListener('input', this.onChange);

        this.pairInput.addEventListener('counterValueChange', this.onChange);
        this.pairInput.addEventListener('input', this.onChange);

        this.onChange();
    }

    onChange = () => {
        const counterValue = parseInt(this.counterInput.value);
        const pairValue = parseInt(this.pairInput.value);

        if (pairValue > counterValue) this.pairInput.value = counterValue;

        toggleState(this.wrapper, STATES.show, counterValue > 0);
    }

    static initGlobal = () => {
        const counters = document.querySelectorAll(`[${SELECTORS.counterInput.el}]`);
        counters.forEach((counter) => new CounterPairs(counter));
    }
}

export default CounterPairs;