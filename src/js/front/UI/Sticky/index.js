import StickyNPM from 'sticky-js';

class Sticky {

    static initGlobal = () => {
        new StickyNPM('[data-action~="sticky"');
    }
}

export default Sticky;
