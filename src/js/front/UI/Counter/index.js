import {getElement, getElements} from '../../../common/helpers/Element';

class Counter {
    constructor(el) {
        if (!el) return;

        this.counterEl = el;
        this.counterInput = getElement('counter-input', this.counterEl);

        this.counterIncrement = getElement('counter-increment', this.counterEl);
        this.counterDecrement = getElement('counter-decrement', this.counterEl);

        this.event = new Event('counterValueChange');

        this.counterIncrement.addEventListener('click', this.increment.bind(this));
        this.counterDecrement.addEventListener('click', this.decrement.bind(this));
    }

    increment = () => {
        this.counterInput.value = (parseInt(this.counterInput.value) || 0) + 1;
        this.counterInput.dispatchEvent(this.event);
    };

    decrement = () => {
        const result = (parseInt(this.counterInput.value) || 0) - 1;
        this.counterInput.value = result < 0 ? 0 : result;
        this.counterInput.dispatchEvent(this.event);
    };

    static initGlobal = () => {
        const counters = getElements('counter');
        counters.forEach((counter) => new Counter(counter));
    }
}

export default Counter;
