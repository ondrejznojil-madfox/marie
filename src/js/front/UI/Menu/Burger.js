import {getElement} from '../../../common/helpers/Element';
import {removeState, setState} from '../../../common/helpers/State';

const STATES = {
    OPENED: 'opened'
};

class Burger {

    constructor() {
        this.el = getElement('burger');
    }

    open = () => {
        setState(this.el, STATES.OPENED);
    };

    close = () => {
        removeState(this.el, STATES.OPENED);
    };

    getEl = () => {
        return this.el;
    };
}

export default Burger;
