import Burger from './Burger';
import {getElement} from '../../../common/helpers/Element';
import {hasState, removeState, setState} from '../../../common/helpers/State';
import {disableScrollLock, enableScrollLock} from '../../../common/helpers/ScrollLock';

const STATES = {
    OPENED: 'opened'
};

class Menu {
    constructor() {
        this.menuEl = getElement('menu');
        if (!this.menuEl) return;

        this.burger = new Burger();

        this.menuTriggers = [this.burger.getEl()];

        this.menuTriggers.forEach((trigger) => {
            trigger && trigger.addEventListener('click', this.toggleMenu.bind(this));
        });
    }

    toggleMenu = () => {
        if (hasState(this.menuEl, STATES.OPENED)) {
            disableScrollLock();
            this.close();
            this.burger.close();
        } else {
            enableScrollLock();
            this.open();
            this.burger.open();
        }
    };

    open = () => {
        setState(this.menuEl, STATES.OPENED);
    };

    close = () => {
        removeState(this.menuEl, STATES.OPENED);
    };
}

export default Menu;
