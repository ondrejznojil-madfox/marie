import { setState, removeState } from '../../../common/helpers/State';

class AnimateTarget {
    static PROPS = {
        type: 'animateType',
        duration: 'animateDuration',
        delay: 'animateDelay'
    }

    constructor(target) {
        if (!target) throw Error('Animatetarget is not defined');

        this.target = target;
        this.duration = parseInt(target.dataset[AnimateTarget.PROPS.duration]) || 500;
        this.delay = parseInt(target.dataset[AnimateTarget.PROPS.delay]) || 0;
    }

    animate = () => {
        this.target.style.transitionDuration = `${this.duration}ms`;
        this.target.style.transitionDelay = `${this.delay}ms`;

        setState(this.target, 'animating');

        setTimeout(() => {
            removeState(this.target, 'animating');
            setState(this.target, 'animated');
            this.clear();
        }, this.duration + this.delay);
    }

    clear = () => {
        this.target.style.removeProperty('transition-duration');
        this.target.style.removeProperty('transition-delay');
        this.target.style.removeProperty('transform');
    }
}

export default AnimateTarget;
