import {getElements} from '../../../common/helpers/Element';

class TotalPrice {
    constructor(formEl, buttonToTrigger) {
        if (!formEl || !buttonToTrigger) return;

        this.formEl = formEl;
        this.buttonToTrigger = buttonToTrigger;

        this.formInputs = getElements('counter-input', this.formEl);

        this.formInputs.forEach(this.bindBtn.bind(this));
    }

    bindBtn = (button) => {
        button.addEventListener('input', this.redraw.bind(this));
        button.addEventListener('counterValueChange', () => {
            this.buttonToTrigger.click();
        });
    };

    redraw = () => {
        this.buttonToTrigger.click();
    };
}

export default TotalPrice;
