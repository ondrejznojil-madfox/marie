import {loadScript} from '../../../common/helpers/LoadScript';
import areaPoints from './areaPoints';

class DeliveryMap {
    constructor() {

        this.initLoader();
    }

    initLoader = async () => {
        await loadScript('https://api.mapy.cz/loader.js');

        Loader.async = true;
        Loader.load(null, null, this.draw.bind(this));
    };

    draw = () => {
        const map = new SMap(JAK.gel('delivery-map'));
        map.addDefaultLayer(SMap.DEF_BASE).enable();

        const mouse = new SMap.Control.Mouse(SMap.MOUSE_PAN | SMap.MOUSE_ZOOM);
        map.addControl(mouse);

        const sync = new SMap.Control.Sync();
        map.addControl(sync);

        const zoom = new SMap.Control.Zoom({title: 'Zoom mapy'});
        map.addControl(zoom, {zIndex: 10});

        const compass = new SMap.Control.Compass({title: 'Posun mapy'});
        map.addControl(compass, {right: '10px', top: '50px'});

        const layer = new SMap.Layer.Geometry();
        map.addLayer(layer);
        layer.enable();

        const options = {
            color: '#d83d50',
            opacity: 0.2,
            outlineColor: '#d83d50',
            outlineOpacity: 0.6,
            outlineWidth: 3,
            curvature: 0
        };

        const coords = areaPoints.map(({lat, lon}) => SMap.Coords.fromWGS84(lat, lon));

        const polygon = new SMap.Geometry(SMap.GEOMETRY_POLYGON, null, coords, options);
        layer.addGeometry(polygon);

        const cz = map.computeCenterZoom(coords);
        map.setCenterZoom(cz[0], cz[1]);
    };
}

export default DeliveryMap;
