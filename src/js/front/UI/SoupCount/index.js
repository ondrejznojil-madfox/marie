import {getElements, getElement} from '../../../common/helpers/Element';

class SoupCount {
    constructor(dayEl) {
        if (!dayEl) return;
        this.dayEl = dayEl;

        this.soupInput = getElement('order-meals-soup-input', this.dayEl);
        if (!this.soupInput) return;

        this.mealInputs = getElements('order-meals-meal-input', this.dayEl);

        this.mealInputs.forEach((input) => {
            input.addEventListener('input', this.calculate.bind(this));
            input.addEventListener('counterValueChange', this.calculate.bind(this));
        });

        this.calculate();
    }

    calculate = () => {
        if (!this.soupInput.disabled) return;
        let sum = 0;

        this.mealInputs.forEach((input) => sum += parseInt(input.value));

        this.soupInput.value = sum;
    };

    static initGlobal = () => {
        const days = getElements('order-meals-day');
        days.forEach((day) => new SoupCount(day));
    };
}

export default SoupCount;
