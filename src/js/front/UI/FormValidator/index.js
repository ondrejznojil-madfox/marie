import gsap, { Power2 } from 'gsap/all';
import { ScrollToPlugin } from 'gsap/ScrollToPlugin';
import { validateElement } from '@mdfx/nette-form-validator';
import {isInViewport} from "../../../common/helpers/IsInViewport";

gsap.registerPlugin(ScrollToPlugin);

const NetteValidator = (() => {
  let classes = {
    inputParent: 'o-form-error',
    label: 'o-form-error__label',
    input: 'o-form-error__input',
    message: 'o-form-error__message',
    messageAbsolute: 'o-form-error__message--absolute'
  };

  const init = (el, classList = {}) => {
    classes = {...classes, ...classList};

    if (el.tagName !== 'FORM') {
      console.error('Element passed in Validator must be form');
      throw 'Element passed in Validator must be form';
    }

    const elementsToValidate = el.querySelectorAll('[data-nette-rules]');

    elementsToValidate.forEach((el) => {
      bindInputEvents(el);
    });

    el.addEventListener('submit', (event) => {
      let firstElWithError = null;

      [...elementsToValidate].map((el) => {
        const error = validateEl(el);

        if (error) {
          event.preventDefault();
          firstElWithError = firstElWithError || el;
        }
      });

      scrollTo(firstElWithError);
    });
  };

  const bindInputEvents = (el) => {
    el.addEventListener('change', () => {validateEl(el);});
    el.addEventListener('keyup', () => {validateEl(el);});
  };

  const validateEl = (el) => {
    const errorMessage = validateElement(el);

    const message = generateMessage(errorMessage);
    message.dataset.formError = 'true';

    if (el.parentNode.dataset.messageType && el.parentNode.dataset.messageType === 'absolute') {
      message.classList.add(classes.messageAbsolute);
    }

    const actualError = el.parentNode.querySelector('[data-form-error]');

    if (errorMessage && actualError) {
        actualError.remove();
        el.parentNode.appendChild(message);
        addClasses(el.parentNode, el.parentNode.querySelector('label'), el, message);
    }

    if (errorMessage && !actualError) {
      el.parentNode.appendChild(message);
      addClasses(el.parentNode, el.parentNode.querySelector('label'), el, message);
    }

    if ( ! errorMessage && actualError) {
      actualError.remove();
      removeClasses(el.parentNode, el.parentNode.querySelector('label'), el, message);
    }

    return errorMessage;
  };


  const generateMessage = (text) => {
    const messageEl = document.createElement('span');
    messageEl.innerHTML = text;
    return messageEl;
  };

  const addClasses = (parent, label, input, message) => {
    parent.classList.add(classes.inputParent);
    label.classList.add(classes.label);
    input.classList.add(classes.input);
    message.classList.add(classes.message);
  };

  const removeClasses = (parent, label, input, message) => {
    parent.classList.remove(classes.inputParent);
    label.classList.remove(classes.label);
    input.classList.remove(classes.input);
    message.classList.remove(classes.message);
  };

  const scrollTo = (el) => {
    if (!isInViewport(el)) {
      gsap.to(window, {duration: .5, scrollTo: {y: el, offsetY: 50}, ease: Power2.easeInOut});
    }
  }

  return {
    init
  };
})();

export default NetteValidator;
