import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import EssentialsPlugin from '@ckeditor/ckeditor5-essentials/src/essentials';
import BoldPlugin from '@ckeditor/ckeditor5-basic-styles/src/bold';
import ItalicPlugin from '@ckeditor/ckeditor5-basic-styles/src/italic';
import BlockQuotePlugin from '@ckeditor/ckeditor5-block-quote/src/blockquote';
import HeadingPlugin from '@ckeditor/ckeditor5-heading/src/heading';
import LinkPlugin from '@ckeditor/ckeditor5-link/src/link';
import ListPlugin from '@ckeditor/ckeditor5-list/src/list';
import ParagraphPlugin from '@ckeditor/ckeditor5-paragraph/src/paragraph';

const ArticleController = (() => {
    ClassicEditor.builtinPlugins = [
        EssentialsPlugin,
        BoldPlugin,
        ItalicPlugin,
        BlockQuotePlugin,
        HeadingPlugin,
        LinkPlugin,
        ListPlugin,
        ParagraphPlugin,
    ];

    ClassicEditor.defaultConfig = {
        toolbar: {
            items: [
                'heading',
                '|',
                'bold',
                'italic',
                'link',
                'bulletedList',
                'numberedList',
                'blockQuote',
                'undo',
                'redo',
            ]
        },
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading2', view: 'h2', title: 'Nadpis 2', class: 'ck-heading_heading2' },
                { model: 'heading3', view: 'h3', title: 'Nadpis 3', class: 'ck-heading_heading3' }
            ]
        },
        language: 'cs',
        defaultLanguage: 'cs',
    };


    const init = () => {
        const elements = document.getElementsByClassName('wysiwyg-editor');

        Array.from(elements).forEach(element => {
            const imageHandlerUrl = element.dataset.imageHandlerUrl

            ClassicEditor.create(element, {
                simpleUpload: {
                    uploadUrl: imageHandlerUrl
                }
            })
        })
    }

    return {
        init
    }
})()

export default ArticleController
