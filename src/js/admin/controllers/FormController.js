import FormValidator from '../UI/FormValidator';
import {getElements} from "../../common/helpers/Element";
import {toggleState} from "../../common/helpers/State";

const FormController = (() => {

    const classes = {
        inputParent: 'o-form-error',
        label: 'o-form-error__label',
        input: 'o-form-error__input',
        message: 'o-form-error__message',
        messageAbsolute: 'o-form-error__message--absolute'
    };

    const init = () => {
        document.querySelectorAll('form').forEach(element => FormValidator.init(element, classes));

        customCheckboxes();
    };

    const customCheckboxes = () => {
        const checkboxes = getElements('custom-checkbox');

        checkboxes.forEach((checkbox) => {
            const input = checkbox.querySelector('input');

            input.addEventListener('change', () => {
                toggleState(checkbox, 'checked', input.checked);
            });
        });
    };

    return {
        init
    };
})();

export default FormController;
