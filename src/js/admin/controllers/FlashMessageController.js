import { bindCloseBtn } from "../UI/FlashMessage/DOMEvents";

const FlashMessageController = (() => {

    const init = () => {
        const DOMMessages = document.querySelectorAll('[data-flash-message]');

        DOMMessages.forEach(message => {
            bindCloseBtn(message);
        });
    };

    return {
        init
    };
})();

export default FlashMessageController;