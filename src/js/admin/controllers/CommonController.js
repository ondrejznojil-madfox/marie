import naja from 'naja';
import {getElements} from '../../common/helpers/Element';
import Input from '../UI/Input';
import Select from '../UI/Select';
import Tabs from '../UI/Tabs';

const CommonController = (() => {

    const init = () => {
        initNaja();
        initInputs();

        Select.initGlobal();
        Tabs.initGlobal();
    };

    const initNaja = () => {
        naja.initialize({
            history: false,
        });
    };

    const initInputs = () => {
        const selects = getElements('basic-select');

        selects.forEach(Input.initSelect);
    };

    return {
        init
    };
})();

export default CommonController;
