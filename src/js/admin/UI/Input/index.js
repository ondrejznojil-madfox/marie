import Choices from "choices.js";

const Input = (() => {

    const initSelect = (el) => {
        if (!el) return;

        return new Choices(el, {
            shouldSort: false,
        });
    };

    return {
        initSelect
    };
})();

export default Input;
