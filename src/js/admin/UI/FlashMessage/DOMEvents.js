
export const bindCloseBtn = (message) => {
    const closeBtn = message.querySelector('.js-close-button');

    if (!closeBtn) return;

    closeBtn.addEventListener('click', () => {
        message.remove();
    });
};
