import Choices from 'choices.js';

class Select {
    static ACTION_SLUG = 'init-choices'

    constructor(element) {
        new Choices(element, {
            removeItemButton: true,
            loadingText: 'Načítám...',
            noResultsText: 'Nic nenalezeno',
            noChoicesText: 'Žádné položky na výběr',
            itemSelectText: 'Vybrat',
            addItemText: (value) => {
                return `Zmáčkněte ENTER pro přídání <b>"${value}"</b>`;
            },
            maxItemText: (maxItemCount) => {
                return `Jen ${maxItemCount} hodnot může být přidáno`;
            },
        });
    }

    static initGlobal() {
        const elements = document.querySelectorAll(`[data-action="${Select.ACTION_SLUG}"]`);

        elements.forEach((element) => new Select(element));
    }
}

export default Select;
