import { validateElement } from "@mdfx/nette-form-validator";

const NetteValidator = (() => {
  let classes = {
    inputParent: 'c-input__wrapper--error',
    label: 'c-input__label--error',
    input: 'c-input__field--error',
    message: 'c-input__error-msg',
    messageAbsolute: 'c-input__error-msg--absolute'
  };

  const init = (el, classList = {}) => {
    classes = {...classes, ...classList};

    if (el.tagName !== 'FORM') {
      console.error('Element passed in Validator must be form');
      throw 'Element passed in Validator must be form';
    }

    const validatorElements = el.querySelectorAll('[data-nette-rules]');

    validatorElements.forEach((el) => {
      bindInputEvents(el);
    });

    el.addEventListener('submit', (event) => {
      validatorElements.forEach((el) => {
        const error = validateEl(el);

        if (error) {
          event.preventDefault();
        }
      });
    });

  };

  const bindInputEvents = (el) => {
    el.addEventListener('change', () => {validateEl(el);});
    el.addEventListener('keyup', () => {validateEl(el);});
  };

  const validateEl = (el) => {
    const errorMessage = validateElement(el);

    const message = generateMessage(errorMessage);
    message.dataset.formError = 'true';

    if (el.parentNode.dataset.messageType && el.parentNode.dataset.messageType === 'absolute') {
      message.classList.add(classes.messageAbsolute);
    }

    const actualError = el.parentNode.querySelector('[data-form-error]');

    if (errorMessage && actualError) {
        actualError.remove();
        el.parentNode.appendChild(message);
        addClasses(el.parentNode, el.parentNode.querySelector('label'), el, message);
    }

    if (errorMessage && !actualError) {
      el.parentNode.appendChild(message);
      addClasses(el.parentNode, el.parentNode.querySelector('label'), el, message);
    }

    if ( ! errorMessage && actualError) {
      actualError.remove();
      removeClasses(el.parentNode, el.parentNode.querySelector('label'), el, message);
    }

    return errorMessage;
  };


  const generateMessage = (text) => {
    const messageEl = document.createElement('span');
    messageEl.innerHTML = text;
    return messageEl;
  };

  const addClasses = (parent, label, input, message) => {
    parent.classList.add(classes.inputParent);
    label.classList.add(classes.label);
    input.classList.add(classes.input);
    message.classList.add(classes.message);
  };

  const removeClasses = (parent, label, input, message) => {
    parent.classList.remove(classes.inputParent);
    label.classList.remove(classes.label);
    input.classList.remove(classes.input);
    message.classList.remove(classes.message);
  };

  return {
    init
  };
})();

export default NetteValidator;
