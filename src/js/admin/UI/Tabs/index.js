import {removeState, setState} from '../../../common/helpers/State';

const DATASET = {
    tabs: 'tabs',
    tabKey: 'tabsKey',
};

const SELECTORS = {
    element: `[data-${DATASET.tabs}]`,
    tabItem: (name) => `[data-${DATASET.tabs}-item=${name}]`,
    button: (name) => `[data-${DATASET.tabs}-button=${name}]`,
};

const STATES = {
    tabActive: 'active',
    buttonActive: 'active',
};

class Tabs {
    constructor({element}) {
        if (!element) throw new Error('Tabs: elements in not provided in constructor');
        this.tabsEl = element;
        this.name = this.tabsEl.dataset[DATASET.tabs];
        if (!this.name) throw new Error('Tabs: name was not defined in data-tab="name"');

        this.tabs = this.fetchPairs(this.tabsEl.querySelectorAll(SELECTORS.tabItem(this.name)), DATASET.tabKey);
        this.buttons = this.fetchPairs(document.querySelectorAll(SELECTORS.button(this.name)), DATASET.tabKey);

        this.lastActiveKey = null;

        this.switchTo(this.tabs.keys().next().value);

        this.bindControls();
    }

    bindControls = () => {
        this.buttons.forEach((element, key) => {
            element.addEventListener('click', (e) => {
                e.preventDefault();
                this.switchTo(key);
            })
        });
    };

    switchTo = (key) => {
        const newTab = this.tabs.get(key);
        const newButton = this.buttons.get(key);

        if (newTab && (key !== this.lastActiveKey)) {
            this.resetAllStates();
            setState(newTab, STATES.tabActive);
            setState(newButton, STATES.buttonActive);
            this.lastActiveKey = key;
        }
    };

    resetAllStates = () => {
        this.tabs.forEach((tab) => removeState(tab, STATES.tabActive));
        this.buttons.forEach((button) => removeState(button, STATES.buttonActive));
    };

    fetchPairs = (nodeArray, dataset) => {
        return new Map([...nodeArray].map((item) => ([item.dataset[dataset], item])));
    };

    static initGlobal = () => {
        const tabs = document.querySelectorAll(SELECTORS.element);
        tabs.forEach((tab) => new Tabs({element: tab}));
    }
}

export default Tabs;
