import ArticleController from "./admin/controllers/ArticleController";
import CommonController from "./admin/controllers/CommonController";
import FlashMessageController from "./admin/controllers/FlashMessageController";
import FormController from "./admin/controllers/FormController";

class Bootstrap {

    static init() {
        ArticleController.init();
        CommonController.init();
        FlashMessageController.init();
        FormController.init();
    }
}

if (document.readyState === 'loading') {
    window.addEventListener('DOMContentLoaded', Bootstrap.init);
} else {
    Bootstrap.init();
}
