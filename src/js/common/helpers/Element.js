
const DATASET = 'element';

const getElement = (name, parent = document) => {
    return parent.querySelector(`[data-${DATASET}~="${name}"]`);
};

const getElements = (name, parent = document) => {
    return parent.querySelectorAll(`[data-${DATASET}~="${name}"]`);
};

export {
    getElement,
    getElements
};
