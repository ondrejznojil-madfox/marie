
export class GoogleMapsApi {

    constructor() {
        // api key for google maps
        this.apiKey = 'AIzaSyAdzTo97uWuHGiuyZNSvw0Kz5WPYAY8mGY';

        // set a globally scoped callback if it doesn't already exist
        if (!window._GoogleMapsApi) {
            this.callbackName = '_GoogleMapsApi.mapLoaded';
            window._GoogleMapsApi = this;
            window._GoogleMapsApi.mapLoaded = this.mapLoaded.bind(this);
        }
    }

    load() {
        if (!this.promise) {
            this.promise = new Promise(resolve => {
                this.resolve = resolve;
                if (typeof window.google === 'undefined') {
                    const script = document.createElement('script');
                    script.src = `//maps.googleapis.com/maps/api/js?key=${this.apiKey}&callback=${this.callbackName}`;
                    script.async = true;
                    document.body.append(script);
                } else {
                    this.resolve();
                }
            });
        }

        return this.promise;
    }

    mapLoaded() {
        if (this.resolve) {
            this.resolve();
        }
    }
}