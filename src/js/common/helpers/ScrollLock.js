
const enableScrollLock = (withOffset = true) => {
    const top = window.pageYOffset || document.documentElement.scrollTop;

    document.body.style.overflow = 'hidden';
    document.body.style.position = 'fixed';
    document.body.style.width = '100%';


    if (withOffset) {
        document.body.style.top = `-${top}px`;
        document.body.dataset.offsetTop = top.toString();
    }
};

const disableScrollLock = (withOffset = true) => {
    document.body.style.removeProperty('overflow');
    document.body.style.removeProperty('position');
    document.body.style.removeProperty('width');
    document.body.style.removeProperty('top');

    if (withOffset) {
        document.documentElement.scrollTop = parseInt(document.body.dataset.offsetTop);
        document.body.dataset.offsetTop = null;
    }
};

export {
    enableScrollLock,
    disableScrollLock
};
