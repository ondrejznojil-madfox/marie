const scrollDetection = () => {
  const eventDown = new Event('scrollDown');
  const eventUp = new Event('scrollUp');
  let lastScrollTop = 0;

  window.addEventListener("scroll", () => { 
    let st = window.pageYOffset || document.documentElement.scrollTop; 

    if (st > lastScrollTop){
        document.dispatchEvent(eventDown);
    } else {
        document.dispatchEvent(eventUp);
    }
    lastScrollTop = st <= 0 ? 0 : st; 
  });
};

export {
  scrollDetection
};