// Polyfills
import '@babel/polyfill';
import BaseController from './front/controllers/BaseController';
import ChunkController from './front/controllers/ChunkController';
import ImageController from './front/controllers/ImageController'
import naja from 'naja'

class App {

    static init = () => {
        const imageController = new ImageController();

        naja.addEventListener('complete', () => {
            imageController.destroy()
            imageController.init()
        })

        naja.addEventListener('before', () => {
            const loader = document.getElementById('loader')
            loader.classList.remove('hidden')
        })

        naja.addEventListener('complete', () => {
            const loader = document.getElementById('loader')
            loader.classList.add('hidden')
        })

        imageController.init()
        new BaseController();
        new ChunkController();
        new ImageController();
    };
}

if (document.readyState === 'loading') {
    window.addEventListener('DOMContentLoaded', App.init);
} else {
    App.init();
}
