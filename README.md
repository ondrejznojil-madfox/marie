Rozjetí projektu
=============
1. Naklonovat si repozitář lokálně `https://gitlab.com/madfoxdesign/camposcatering-vyvarovna`
2. Překopírovat soubor `app/config/local.neon.sample` a přejmenovat ho na `app/config/local.neon`. V tomto souboru je uložená lokální konfigurace Nette projektu a přejmenováním dáme projektu vědět, že chceme používat development mód.
3. Lokálně nastavit nejvyšší práva složkám `temp` a `dir`. Konkrétně se jedná o příkazy `chmod -R 777 /temp` a `chmod -R 777 /log`.
4. Spustit příkaz `docker-compose up --build`. Pokud vše proběhne v pořádku, na adrese `localhost:8080` by se měla zobrazit úvodní stránka.
5. Lokálně spustit příkaz gulp a začít pracovat.


Nastavení PHPStormu pro PHP vývoj 
=============

 - Nastavit verzi jazyka PHP na 7.4
 - Zakázat inspekce na FQN (Fully qualified names)
 - Zapnout option "Ensure line feed at file end on Save"
 - Nastavit globálně taby pro indentaci pro všechny druhy souborů 

Statická kontrola před puhsnutím
=============

Kontrola PHPstan, Lintu a Codingstandard probíhá v CI prostředí gitlabu. Pokud kontroly neprojdou, Gitlab CI pipeline
failne. Abychom tomuto failování přecházeli. Před každým commitem spoustíme statickou analýzu lokálně přes docker:

**Backendový vývoj:**
 - `./tools/cs.sh`
 - `./tools/phpstan.sh`
 
**Front-endový vývoj:**
 - `./tools/eslint.sh`
 - `./tools/eslint-fix.sh`
