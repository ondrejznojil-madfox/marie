#!/usr/bin/env bash

echo "Slack notify start"
./tools/slack_notify.sh -s "CamposCatering Production"

# current status
COMMIT_OLD=$(git rev-parse HEAD)

CHECKOUT_COUNT=0

yarn run cleanBuild

git status

until git checkout master
do
  sleep 0.1
  CHECKOUT_COUNT=$((CHECKOUT_COUNT+1))

    if [[ CHECKOUT_COUNT -eq 10 ]]; then
        echo "Deploy failed"
        exit 1
    fi
done

PULL_COUNT=0

until git pull origin master
do
    PULL_COUNT=$((PULL_COUNT+1))

    if [[ $PULL_COUNT -eq 10 ]]; then
        echo "Deploy failed"
        exit 1
    fi

    sleep 0.1
done

echo "Spouštím composer"
php8.0 composer.phar install --no-dev

echo "Instaluji NPM závislosti"
yarn install --no-audit

echo "Spouštím front-end build"
yarn run build

echo "Upgraduji verzi assetů"
php8.0 tools/upgradeAssetVersion.php

echo "Mažu cache"
rm -rf ./temp/cache/Nette.robotLoader/*
rm -rf ./temp/cache/Nette.configurator/*
rm -rf ./temp/cache/latte/*
