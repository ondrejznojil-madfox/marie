INSERT INTO image (id, url, width, height, media_key) VALUES
(1, "https://znojil-private.s3.eu-central-1.amazonaws.com/testCase1.jpeg", 1600, 1067, "testCase1.jpeg"),
(2, "https://znojil-private.s3.eu-central-1.amazonaws.com/testCase2.jpeg", 1600, 900, "testCase2.jpeg");

INSERT INTO test_case (image_id, `position`, `name`) VALUES
(1, 1, 'Krásná zelená příroda'),
(2, 2, 'Hrad a tak');
