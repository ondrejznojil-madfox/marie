INSERT INTO user (email, password, user_role_id) VALUES
("user@user.cz", "$2y$12$7GdWx4sMBwAQpS8UJxOMbexFTzC3aTWPPEAUGalwtN9vrN1mFp8.S", 1),
("admin@admin.cz", "$2y$12$7GdWx4sMBwAQpS8UJxOMbexFTzC3aTWPPEAUGalwtN9vrN1mFp8.S", 2);
