<?php declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

$container = App\Bootstrap::boot()
	->addParameters([
		'wwwDir' => __DIR__ . '/../www/'
	])
	->createContainer();

$decoded = \Nette\Neon\Neon::decode(file_get_contents(__DIR__ . '/../app/config/local.neon'));

$assetsVersion = $decoded['parameters']['assetsVersion'] ?? '0';

$decoded['parameters']['assetsVersion'] = (string) ++$assetsVersion;

file_put_contents(__DIR__ . '/../app/config/local.neon', \Nette\Neon\Neon::encode($decoded, 1));

exit(0);
