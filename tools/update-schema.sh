ENV="docker"

if [ "$1" = "native" ] ; then
	ENV=$1
fi

if [ "$ENV" = "native" ]; then
    php7.4 vendor/doctrine/orm/bin/doctrine orm:schema-tool:drop --force
    php7.4 vendor/doctrine/orm/bin/doctrine orm:schema-tool:create

    php7.4 vendor/doctrine/orm/bin/doctrine dbal:import tools/data/userRole.sql
    php7.4 vendor/doctrine/orm/bin/doctrine dbal:import tools/data/user.sql
    php7.4 vendor/doctrine/orm/bin/doctrine dbal:import tools/data/testCase.sql
else
    docker exec -i marie_php vendor/doctrine/orm/bin/doctrine orm:schema-tool:drop --force
    docker exec -i marie_php vendor/doctrine/orm/bin/doctrine orm:schema-tool:create

    docker exec -i marie_php vendor/doctrine/orm/bin/doctrine dbal:import tools/data/userRole.sql
    docker exec -i marie_php vendor/doctrine/orm/bin/doctrine dbal:import tools/data/user.sql
    docker exec -i marie_php vendor/doctrine/orm/bin/doctrine dbal:import tools/data/testCase.sql
fi

