/* eslint-disable */
import gulp from 'gulp';

// gulp modules import
import { clean, cleanDev, staticFiles, vendorFiles } from './.gulptasks/misc';
import { icons } from './.gulptasks/icons';
import { imagesCopy } from './.gulptasks/images';
import { javascripts } from './.gulptasks/webpack';
import { stylesheets } from './.gulptasks/styles';
import { stylesheetsMail } from './.gulptasks/styles.mail';
import { server } from './.gulptasks/server';

export const dev = gulp.series(
    cleanDev,
    gulp.parallel(
        staticFiles,
        vendorFiles,
        icons,
        javascripts,
        stylesheets,
        stylesheetsMail,
        imagesCopy
    ),
    server
);

export const build = gulp.task('build',
  gulp.series(
    cleanDev,
    clean,
    gulp.parallel(
        staticFiles,
        vendorFiles,
        icons,
        javascripts,
        stylesheets,
        stylesheetsMail,
        imagesCopy
    )
));

export const clearBuild = gulp.task('cleanBuild',
    gulp.series(
        clean
    )
);

export default dev;
