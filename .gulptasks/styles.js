/* eslint-disable */
import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'autoprefixer';
import postcss from 'gulp-postcss';
import cleancss from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';
import gulpif from 'gulp-if';
import globalConfig from './config';

const isProduction = (process.env.NODE_ENV === 'production');

const paths = {
    src: [
        './src/scss/admin.scss',
        './src/scss/front.error.scss',
        './src/scss/front.scss',
    ],
    dest: './www/assets/css/',
    destDev: './www/assets-dev/css/',
};

const config = {
    postCss: true,
};

const postCssPlugins = [
    autoprefixer(globalConfig.css.autoprefixer),
];

export const stylesheets = () => gulp.src(paths.src)
    .pipe(sourcemaps.init())
    .pipe(sass(globalConfig.css.sass))
    .on('error', sass.logError)
    .pipe(gulpif(config.postCss, postcss(postCssPlugins)))
    .on('error', () => {console.log('postcss failed')})
    .pipe(gulpif(isProduction, cleancss()))
    .on('error', () => {console.log('cleancss failed')} )
    .pipe(gulpif(!isProduction, sourcemaps.write('.')))
    .pipe(
        isProduction ?
            gulp.dest(paths.dest) :
            gulp.dest(paths.destDev)
    );
