/* eslint-disable */
import gulp from 'gulp';
import gulpCopy from 'gulp-copy';

const isProduction = (process.env.NODE_ENV === 'production');

const config = {
	src: './src/images/**/*',
	dest: './www/assets/images/',
	destDev: './www/assets-dev/images/',
};

export const imagesCopy = () => gulp.src(config.src)
	.pipe(
		isProduction ?
			gulpCopy(config.dest, {prefix: 2}) :
			gulpCopy(config.destDev, {prefix: 2})
	);
