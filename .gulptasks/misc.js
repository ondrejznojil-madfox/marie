/* eslint-disable */
import gulp from 'gulp';
import gulpClean from 'gulp-clean';
import gulpCopy from 'gulp-copy';

const isProduction = (process.env.NODE_ENV === 'production');

export const clean = () => gulp.src('./www/assets/*', {read: false})
    .pipe(gulpClean());

export const cleanDev = () => gulp.src('./www/assets-dev/*', {read: false})
    .pipe(gulpClean());

export const staticFiles = () => gulp.src('./src/static/**/*')
    .pipe(
        isProduction ?
            gulpCopy('./www/assets/static/', {prefix: 2}) :
            gulpCopy('./www/assets-dev/static/', {prefix: 2})
    );

export const vendorFiles = () => gulp.src('./src/vendor/**/*')
    .pipe(
        isProduction ?
            gulpCopy('./www/assets/vendor/', {prefix: 2}) :
            gulpCopy('./www/assets-dev/vendor/', {prefix: 2})
    );
