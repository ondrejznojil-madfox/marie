/* eslint-disable */
import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'autoprefixer';
import postcss from 'gulp-postcss';
import gulpif from 'gulp-if';

const paths = {
    src: './src/scss/front.mail.scss',
    dest: './app/MailModule/assets/',
};

const config = {
    autoprefixer: {
        browsers: ['Explorer >= 10', 'iOs >= 7', 'last 3 versions'],
    },
    sass: {
        outputStyle: 'expanded',
        includePath: ['node_modules']
    },
    postCss: true,
};

const postCssPlugins = [
    autoprefixer(config.autoprefixer),
];

export const stylesheetsMail = () => gulp.src(paths.src)
    .pipe(sass(config.sass))
    .on('error', sass.logError)
    .pipe(gulpif(config.postCss, postcss(postCssPlugins)))
    .on('error', () => {console.log('postcss failed')})
    .pipe(gulp.dest(paths.dest));
