/* eslint-disable */
import path from 'path';
import fs from 'fs'
import webpack from 'webpack';
import process from 'process';

const isProduction = (process.env.NODE_ENV === 'production');
const entryMap = {};

const __root = path.resolve(__dirname, '../');

const { styles } = require( '@ckeditor/ckeditor5-dev-utils' );

fs.readdirSync(path.resolve(__dirname, '../src/js'))
  .filter(file => {
      return file.match(/.*\.js$/);
  })
  .forEach(f => {
      entryMap[f.replace(/\.js$/, '')] = [path.resolve(__dirname, '../src/js/' + f)];
  });

let config = {

    mode: isProduction ? 'production' : 'development',
    context: path.resolve(__dirname, '.'),
    watch: ! isProduction,

    entry: entryMap,

    output: {
        filename: isProduction ? './assets/js/[name].js' : './assets-dev/js/[name].js',
        path: path.resolve(__dirname, '../www'),
        chunkFilename: isProduction ? './assets/js/chunks/[name].[contenthash].chunk.js' : './assets-dev/js/chunks/[name].[contenthash].chunk.js',
        publicPath: '/'
    },

    resolve: {
        modules: [
            path.resolve('node_modules'),
        ],
        extensions: ['.js', '.jsx', '.json'],
    },

    module: {
        rules: [
            // {
            //     enforce: 'pre',
            //     test: /\.js$/,
            //     exclude: /node_modules/,
            //     loader: 'eslint-loader',
            // },
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-syntax-dynamic-import']
                    }
                }
            },
            {
                test: /ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/,
                use: [ 'raw-loader' ]
            },
            {
                test: /ckeditor5-[^/\\]+[/\\]theme[/\\].+\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'singletonStyleTag',
                            attributes: {
                                'data-cke': true
                            }
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: styles.getPostCssConfig( {
                            themeImporter: {
                                themePath: require.resolve( '@ckeditor/ckeditor5-theme-lark' )
                            },
                            minify: true
                        } )
                    },
                ]
            }
        ]
    }
};

const javascripts = () => {

    return new Promise(resolve => webpack(config, (err) => {
        if (err) console.log('Webpack', err);

        resolve();
    }));
};

module.exports = { config, javascripts };
