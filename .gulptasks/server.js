/* eslint-disable */
import gulp from 'gulp';
import Browser from 'browser-sync';
import webpack from 'webpack';
import gulpNotify from 'gulp-notify';
import { stylesheets } from './styles';
import { stylesheetsMail } from './styles.mail';
import { icons } from './icons';
import webpackDevMiddleware from 'webpack-dev-middleware';

import { config as webpackConfig } from './webpack';

export const browser = Browser.create();
const bundler = webpack(webpackConfig);

function reload(done) {
    browser.reload();
    done();
}

export const server = () => {

    const config = {
        proxy: "localhost:8080",
        port: 3000,
        open: false,
        middleware: [
            webpackDevMiddleware(bundler, { /* options */ }),
        ]
    };

    browser.init(config);

    gulp.src('.')
        .pipe(gulpNotify('Server running on localhost'));

    gulp.watch('./src/js/**/*.js').on('change', () => browser.reload());
    gulp.watch('./src/icons/**/*.svg').on('change', () => icons().pipe(browser.stream({match: '**/*.svg'})));
    gulp.watch('./src/scss/**/*.scss').on('change', () => {
        stylesheets().pipe(browser.stream({match: '**/*.css'}));
        stylesheetsMail().pipe(browser.stream({match: '**/*.css'}));
    });
    gulp.watch('./app/**/*.latte').on('change', () => gulp.series(reload)());
};
