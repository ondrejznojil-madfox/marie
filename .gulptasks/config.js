/* eslint-disable */
const config = {
    css: {
        autoprefixer: {
            browsers: ['Explorer >= 10', 'iOs >= 7', 'last 3 versions'],
        },
        sass: {
            outputStyle: 'expanded',
            includePath: ['node_modules']
        },
    },
    
};

module.exports = config;
