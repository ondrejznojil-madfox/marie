/* eslint-disable */
import gulp from 'gulp';
import gulpSvgstore from 'gulp-svgstore';
import gulpRename from 'gulp-rename';

const isProduction = (process.env.NODE_ENV === 'production');

const paths = {
    srcAdmin: './src/icons/admin/**/*.svg',
    srcFront: './src/icons/front/**/*.svg',
    dest: './www/assets/icons',
    destDev: './www/assets-dev/icons',
};

const iconsAdmin = () => gulp.src(paths.srcAdmin)
    .pipe(gulpSvgstore())
    .pipe(gulpRename('admin.svg'))
    .pipe(
        isProduction ?
            gulp.dest(paths.dest) :
            gulp.dest(paths.destDev)
    );

const iconsFront = () => gulp.src(paths.srcFront)
    .pipe(gulpSvgstore())
    .pipe(gulpRename('front.svg'))
    .pipe(
        isProduction ?
            gulp.dest(paths.dest) :
            gulp.dest(paths.destDev)
    );

export const icons = gulp.parallel(
    iconsAdmin,
    iconsFront
)
